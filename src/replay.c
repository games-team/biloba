/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

/**
 * Biloba - Q1 2005
 * Game by Guillaume Demougeot <dmgt@wanadoo.fr>
 * Code by Colin Leroy <colin@colino.net>
 *
 * This file contains the replay code.
 */

#include <stdio.h>
#include <assert.h>
#include <errno.h>

#include "replay.h"
#include "game.h"
#include "utils.h"

static FILE *replay_fp = NULL;
static int32_t n_players = -1;
static char p_names[4][512];

#define DUMP_HEADER "Biloba-game-dump"
#define DUMP_VERSION 1

int replay_game_setup(const char *replay)
{
	int32_t i, len;
	char header[128];

	replay_fp = fopen(replay, "rb");

	if (replay_fp == NULL)
		return -1;

	if (fread(header, sizeof(char), strlen(DUMP_HEADER), replay_fp) < strlen(DUMP_HEADER)) {
		printf("Failed reading header: %s\n", strerror(errno));
		return -1;
	}

	header[strlen(DUMP_HEADER)] = '\0';
	if (strcmp(header, DUMP_HEADER) != 0) {
		printf("Unexpected header: %s\n", header);
		return -1;
	}

	if (fread(&i, sizeof(i), 1, replay_fp) < 1) {
		printf("Failed reading version: %s\n", strerror(errno));
		return -1;
	}
	
	if (i != DUMP_VERSION) {
		printf("Unexpected version: %d\n", i);
		return -1;
	}

	if (fread(&n_players, sizeof(n_players), 1, replay_fp) < 1) {
		printf("Failed reading number of players: %s\n", strerror(errno));
		return -1;
	}

	if (n_players < 2 || n_players > 4) {
		printf("Unexpected number of players: %d\n", n_players);
		return -1;
	}
	
	printf("%d players.\n", n_players);

	for (i = 0; i < n_players; i++)
	{
		if (fread(&len, sizeof(len), 1, replay_fp) < 1) {
			printf("Failed reading player %d name length: %s\n", i, strerror(errno));
			return -1;
		}

		if (len > 0 && fread(p_names[i], sizeof(char), len, replay_fp) < len) {
			printf("Failed reading player %d name: %s\n", i, strerror(errno));
			return -1;
		}

		p_names[i][len] = '\0';
		printf("Player %d: %s\n", i + 1, p_names[i]);
	}
	return 0;
}

int replay_get_num_players(void)
{
	assert(n_players > 0);

	return n_players;
}

const char *replay_get_player(int i)
{
	assert(i >= 0);
	assert(i < n_players);
	assert(n_players > 0);

	return p_names[i];
}

void replay_get_event(int color, int *x, int *y)
{
	Move move;

	assert(replay_fp != NULL);

        do {
		if (fread(&move, sizeof(move), 1, replay_fp) < 1) {
			printf("read failed: %s (%d)\n", strerror(errno), errno);
			goto err;
		} else {
			printf("Player '%s' plays %d,%d\n", p_names[move.player], move.x, move.y);
		}

		if (move.player != color) {
			printf("wrong player %d, expected %d\n", move.player, color);
			goto err;
		}
	} while (move.x == -1 && move.y == -1);

	*x = move.x;
	*y = move.y;
	return;

err:
	fclose(replay_fp);
	replay_fp = NULL;
	*x = -2;
	*y = -2;
}

int replay_dump_game(Game *game, const char *dump_file)
{
	FILE *fp;
	int32_t i;
	char header[128];

	fp = fopen(dump_file, "wb");
	if (!fp)
		return -1;

	/* Dump header */
	strcpy(header, DUMP_HEADER);
	if (fwrite(header, sizeof(char), strlen(header), fp) < strlen(header))
		return -1;

	/* Dump version */
	i = DUMP_VERSION;
	if (fwrite(&i, sizeof(i), 1, fp) < 1)
		return -1;

	/* Dump number of players */
	if (fwrite(&game->num_players, sizeof(game->num_players), 1, fp) < 1)
		return -1;

	/* Dump players names */
	for (i = 0; i < game->num_players; i++) {
		int32_t n;

		n = game->player_name[i] ? strlen(game->player_name[i]) : 0;

		if (fwrite(&n, sizeof(n), 1, fp) < 1)
			return -1;
		if (n > 0 && fwrite(game->player_name[i], sizeof(char), n, fp) < n)
			return -1;
	}

	/* Dump moves */
	for (i = 0; i <= game->move_id; i++)
		if (fwrite(&game->moves[i], sizeof(Move), 1, fp) < 1)
			return -1;

	fclose(fp);

	return 0;
}
