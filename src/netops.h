/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

/**
 * Biloba - Q1 2005
 * Game by Guillaume Demougeot <dmgt@wanadoo.fr>
 * Code by Colin Leroy <colin@colino.net>
 *
 * This file contains the basic network I/O code.
 */

#ifndef __NETOPS_H__
#define __NETOPS_H__

#define BCAST_LEN (strlen("Biloba") + 32 + 1)

#ifdef __MINGW32__
#define SHUT_RDWR SD_BOTH
#define sleep(seconds) do { Sleep((seconds)*1000); } while (0)
#endif

#define NET_HOST "paperstreet.colino.net"
#define NET_PORT 8000
#define BCAST_PORT 8001

int send_msg(int fd, const void* buf, unsigned char len);
int read_msg(int fd, void *buf, unsigned char avail, unsigned char *len);

int send_int(int fd, int32_t n);
int read_int(int fd, int32_t *n);

int send_char(int fd, char c);
int read_char(int fd, char *c);

int send_str(int fd, const char *str);
int read_str(int fd, char *str, int max_len);

int sock_select(int sock, int timeout_msec);
void sock_close(int sock);
void get_bcast_msg(char *msg, size_t len, int magic_number);
const char *get_hostname(struct sockaddr_in *addr);
int get_net_error(void);
const char *net_err_str(int err);
#endif
