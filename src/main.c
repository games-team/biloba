/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

/**
 * Biloba - Q1 2005
 * Game by Guillaume Demougeot <dmgt@wanadoo.fr>
 * Code by Colin Leroy <colin@colino.net>
 *
 * This file contains all early game initialisation
 * and termination.
 */

#include <stdlib.h>
#include <string.h>
#include <SDL.h>
#include <SDL_image.h>
#include <time.h>
#include <unistd.h>

#include "utils.h"
#include "board.h"
#include "logic.h"
#include "options.h"
#include "net.h"
#include "sound.h"

static void run_tests()
{
#ifdef DEBUG
#endif
}

int main(int argc, char *argv[])
{
	const char *execname = argv[0];
	if (execname[0] >= 'A' && execname[0] <= 'Z' && execname[1] == ':') {
		/* Windows: Skip C: */
		execname += 2;
	}
	if (!strncmp(execname, DIR_SEP, strlen(DIR_SEP))) {
		/* absolute path */
		char *last_dirsep = NULL;
		progpath = strdup(execname);
		last_dirsep = progpath;
		while (strstr(last_dirsep + 1, DIR_SEP) != NULL)
			last_dirsep = strstr(last_dirsep + 1, DIR_SEP);
		*last_dirsep = '\0';
		if (chdir(progpath) < 0)
			printf("Couldn't chdir to %s\n", progpath);
	}
	
	SDL_Init(SDL_INIT_EVERYTHING|SDL_INIT_NOPARACHUTE);
	SDL_EnableUNICODE(1);
	if ( (screen=SDL_SetVideoMode(XS, YS, BPP, FLAGS)) == NULL ) {
		fprintf(stderr, "Couldn't set %dx%dx%d video mode: %s\n", 
				XS, YS, BPP, SDL_GetError());
		exit(1);
	}

	srand(time(NULL));

	sound_init();
	sound_load_sounds();

	atexit(SDL_Quit);
	
#ifndef MAEMO
	SDL_WM_SetCaption("Biloba " VERSION, NULL);
#else
	SDL_WM_SetCaption("Biloba", "Biloba");
	SetWMName("Biloba", (FLAGS & SDL_FULLSCREEN));
	SDL_ShowCursor(0);
#endif	

	run_tests();

	net_init();

	while (!should_quit()) {
		game_init(FALSE);

		if (get_options(argc > 1 ? argv[1]:NULL) < 0)
			break;

		if (should_quit())
			break;

		board_build();

		game_init(TRUE);
		
		play_game();
		
		board_destroy();

		if (argc > 1)
			break;
	}

	sound_stop();

	net_stop();

	exit(0);
}
