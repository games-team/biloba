/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

/**
 * Biloba - Q1 2005
 * Game by Guillaume Demougeot <dmgt@wanadoo.fr>
 * Code by Colin Leroy <colin@colino.net>
 *
 * Our single-linked-list implementation, done to
 * avoid a dependancy to another library.
 */

#include <assert.h>
#include <stdlib.h>
#include "llist.h"

/**
 * Append a pointer to a list
 *
 * @param[in] list 	the list
 * @param[in] data	the data to append
 *
 * @return the pointer to the updated list (which can be different than the
 * original one.
 */
LList *llist_append(LList *list, void *data)
{
	LList *result = malloc(sizeof(LList));
	LList *orig = list;
	result->data = data;
	result->next = NULL;
	if (list) {
		while(list->next)
			list = list->next;
		list->next = result;
		return orig;
	} 

	return result;
}

/**
 * Copy a list to another one
 *
 * @param[in] list 	the list
 *
 * @return the pointer to the copied list
 */
LList *llist_copy(LList *list)
{
	LList *copy = NULL;
	LList *o_cur;
#ifdef DEBUG	
	LList *c_cur;
#endif

	for (o_cur = list; o_cur != NULL; o_cur = o_cur->next)
		copy = llist_prepend(copy, o_cur->data);

	copy = llist_reverse(copy);
#ifdef DEBUG	
	for (o_cur = list, c_cur = copy; o_cur != NULL;
	     o_cur = o_cur->next, c_cur = c_cur->next)
		assert(o_cur->data == c_cur->data);
	
	assert(o_cur == NULL);
	assert(c_cur == NULL);
#endif	
	return copy;
}

/**
 * Reverse a list order
 *
 * @param[in] list 	the list
 *
 * @return the pointer to the modified list
 */
LList *llist_reverse(LList *list)
{
	LList *rev = NULL;

	for (; list != NULL; list = list->next)
		rev = llist_prepend(rev, list->data);
	
	llist_free(list);
	return rev;
}

/**
 * Prepend a pointer to a list
 *
 * @param[in] list 	the list
 * @param[in] data	the data to prepend
 *
 * @return the pointer to the updated list (which will be different than the
 * original one.
 */
LList *llist_prepend(LList *list, void *data)
{
	LList *result = malloc(sizeof(LList));
	result->data = data;
	result->next = list;
	return result;
}

/**
 * Remove a pointer from a list
 *
 * @param[in] list 	the list
 * @param[in] data	the data to remove
 *
 * @return the pointer to the updated list (which can be different than the
 * original one.
 */
LList *llist_remove(LList *list, void *data)
{
	LList *result = list;
	if (!list)
		return NULL;
	
	if (list->data == data) {
		result = list->next;
		free(list);
		return result;
	}
	
	while (list) {
		if (list->next && list->next->data == data) {
			LList *tmp = list->next;
			list->next = list->next->next;
			free(tmp);
			return result;
		}
		list = list->next;
	}
	return result;
}

/**
 * Find a pointer in list
 *
 * @param[in] list 	the list
 * @param[in] data	the data to look for
 *
 * @return the list item containing the data, or NULL if it isn't
 * there.
 */
LList *llist_find(LList *list, void *data)
{
	while(list) {
		if (list->data == data)
			return list;
		list = list->next;
	}
	return NULL;
}

/**
 * Free a list
 *
 * @param[in] list 	the list to free
 */
void llist_free(LList *list)
{
	while (list) {
		LList *next = list->next;
		free(list);
		list = next;
	}
}

/**
 * Get the length of a list
 *
 * @param[in] list 	the list
 *
 * @return the number of items the list contains.
 */
int llist_length(LList *list)
{
	int cnt = 0;
	while (list) {
		cnt++;
		list = list->next;
	}
	return cnt;
}
