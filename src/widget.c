/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

/**
 * Biloba - Q1 2005
 * Game by Guillaume Demougeot <dmgt@wanadoo.fr>
 * Code by Colin Leroy <colin@colino.net>
 *
 * This file contains the widget stuff for the GUI.
 */

#include <stdlib.h>
#include <SDL.h>
#include <SDL_image.h>
#include "utils.h"
#include "llist.h"
#include "font.h"
#include "widget.h"

struct _Widget
{
	/* The filename of the image */
	char *image_name;
	/* The image */
	SDL_Surface *surface;
	
	/* The widget's coords */
	int x;
	int y;
	
	/* The widget's states */
	int visible;
	int enabled;
	
	/* The widget's clicked callback */
	void (*clicked_callback)(struct _Widget *widget, void *data);
	void *clicked_cb_data;

	/* Text handling enabled */
	int is_text;
	/* Current text */
	char *text;
	/* Default text */
	char *default_text;
	/* Maximum storable length */
	int max_text_len;
	/* Is the widget currently in edit mode */
	int is_editing;
	/* Is the widget editable */
	int editable;
	/* X offset to draw the text in the widget */
	int text_x;
	/* Y offset to draw the text in the widget */
	int text_y;
	/* The callback on input end (Enter, Return, Escape) */
	void (*text_validate_callback)(struct _Widget *widget, void *data);
	void *text_validate_cb_data;
	
	/* List handling is enabled */
	int is_list;
	int list_x;
	int list_y;
	/* Callback to load the list of strings */
	int (*get_strings)(struct _Widget *widget, char ***strings);
	/* Callback when a string is selected */
	void (*list_clicked_callback)(struct _Widget *widget, int index,
				      char *string);
	/* Whether the list is shown */
	int is_showing_list;
	/* List width */
	int list_width;
	/* Cached list rectangle boundaries, for selection */
	int cur_list_start_x;
	int cur_list_start_y;
	int cur_list_end_x;
	int cur_list_end_y;
	/* Cached list strings */
	char **list_strings;
	int n_list_strings;
};

static LList *widget_list = NULL;

/**
 * Init a widget so that it's able to display text.
 *
 * @param[in]	widget		The widget
 * @param[in]	default_text	The text to use when the user sets text to NULL
 * @param[in]	max_len		The maximum text length this widget can store
 * @param[in]	x		The X offset inside the widget to start displaying text
 * @param[in]	y		The Y offset inside the widget to start displaying text
 * @param[in]	text_validate_callback
 *				The callback to call when user hits Enter
 * @param[in]	text_validate_cb_data
 *				The callback data
 */
void text_widget_init(Widget *widget, const char *default_text, int max_len,
		      int x, int y, void (*text_validate_callback)
		      (struct _Widget *widget, void *data),
		      void *text_validate_cb_data)
{
	assert(widget->is_text == 0);
	widget->is_text = 1;

	widget->text_x = x;
	widget->text_y = y;

	widget->text = malloc(max_len + 1);
	strncpy(widget->text, default_text, max_len);
	widget->text[max_len] = '\0';

	widget->default_text = malloc(max_len + 1);
	strncpy(widget->default_text, default_text, max_len);
	widget->default_text[max_len] = '\0';

	widget->max_text_len = max_len;
	widget->is_editing = 0;
	widget->editable = 1;
	
	widget->text_validate_callback = text_validate_callback;
	widget->text_validate_cb_data = text_validate_cb_data;
}

/**
 * Sets the editable status of a text-enabled widget
 *
 * @param[in]	widget		The widget
 * @param[in]	editable	The status
 */
void text_widget_set_editable(Widget *widget, int editable)
{
	assert(widget->is_text);
	widget->editable = editable;
}

/**
 * Set the displayed text of a text-enabled widget
 *
 * @param[in]	widget		The widget
 * @param[in]	text		The text
 */
void text_widget_set_text(Widget *widget, const char *text)
{
	const char *txt = text ? text : widget->default_text;
	assert(widget->is_text);
	strncpy(widget->text, txt ? txt : "",
		widget->max_text_len);
	widget->text[widget->max_text_len] = '\0';
}

/**
 * Gets the current text of a text-enabled widget
 *
 * @param[in]	widget		The widget
 *
 * @return the text
 */
const char *text_widget_get_text(Widget *widget)
{
	assert(widget->is_text);
	return widget->text;
}

/**
 * Sets the clicked callback (and callback data) of a widget
 *
 * @param[in]	widget			The widget
 * @param[in]	clicked_callback	The callback
 * @param[in]	data			The callback's data
 */
void widget_set_clicked_callback(Widget *widget,
		      void (*clicked_callback)(Widget *widget, void *data),
		      void *data)
{
	widget->clicked_callback = clicked_callback;
	widget->clicked_cb_data  = data;
}

/**
 * Create a widget
 *
 * @param[in]	img_src			The image to use
 * @param[in]	x			The X coord of the widget
 * @param[in]	y			The Y coord of the widget
 * @param[in]	clicked_callback	The callback to call on click
 * @param[in]	clicked_cb_data		The callback's data
 *
 * @return the new widget
 */
Widget *widget_create(const char *img_src, int x, int y,
		      void (*clicked_callback)(Widget *widget, void *data),
		      void *clicked_cb_data)
{
	Widget *widget = malloc(sizeof(Widget));
	
	widget->image_name = strdup(img_src);
	widget->surface = biloba_load_image(img_src);
	assert(widget->surface);
	widget->x = x;
	widget->y = y;
	widget->is_text = 0;
	widget->is_list = 0;
	widget->clicked_callback = clicked_callback;
	widget->clicked_cb_data  = clicked_cb_data;

	widget->visible = 1;
	widget->enabled = 1;

	/* We prepend, so that the last added widget is "on top". */
	widget_list = llist_prepend(widget_list, widget);
	return widget;
}

/**
 * Sets a widget source image
 *
 * @param[in]	widget			The widget
 * @param[in]	img_src			The image to use
 */
void widget_set_image(Widget *widget, const char *img_src)
{
	SDL_FreeSurface(widget->surface);
	if (img_src) {
		free(widget->image_name);
		widget->image_name = strdup(img_src);
	}
	widget->surface = biloba_load_image(widget->image_name);
	assert(widget->surface);
}

/**
 * Get a semi-opaque surface to disable a widget
 *
 * @return a semi-opaque SDL_surface
 */
static SDL_Surface *disabler(void)
{
	static SDL_Surface *gray = NULL;
	if (gray == NULL)
		gray = biloba_load_image("black-highlighter.png");
	assert(gray);

	return gray;
}

/**
 * Draw a widget's text
 *
 * @param[in]	widget			The widget
 */
static void widget_draw_text(Widget *widget)
{
	draw_message_with_update(widget->text, 
		widget->x + widget->text_x + 4, widget->y + widget->text_y + 8,
		widget->surface->w - widget->text_x - 8, widget->is_editing,
		FALSE);
}

static void list_widget_free_strings(Widget *widget)
{
	int i;

	assert(widget->is_list);

	for (i = 0; i < widget->n_list_strings; i++)
		free(widget->list_strings[i]);
	
	free(widget->list_strings);
	widget->list_strings = NULL;
	widget->n_list_strings = 0;
}

static void list_widget_draw_list(Widget *widget)
{
	int i;
	SDL_Rect rect;

	int start_x, start_y;
	int end_x, end_y;

	assert(widget->is_list);
	assert(widget->get_strings);

	list_widget_free_strings(widget);
	assert(widget->list_strings == NULL);

	widget->n_list_strings = 
		widget->get_strings(widget, &widget->list_strings);

	if (widget->n_list_strings <= 0) {
		widget->list_strings = NULL;
		widget->is_showing_list = FALSE;
		return;
	}
	
	start_x = widget->list_x;
	start_y = widget->list_y;

	end_x = start_x + widget->list_width;
	end_y = start_y + (widget->n_list_strings + 1) * ( LINEHEIGHT + 1);

	/* Offset into screen */
	if (end_x > XS) {
		start_x -= end_x - XS;
		end_x = XS;
	}
	if (start_x < 0) {
		end_x += start_x;
		start_x = 0;
	}
	if (end_y > YS) {
		start_y -= end_y - YS;
		end_y = YS;
	}
	if (start_y < 0) {
		end_y += start_y;
		start_y = 0;
	}
	rect.x = start_x - 1; rect.y = start_y;
	rect.w = end_x - (start_x - 1); rect.h = end_y - start_y;

	SDL_FillRect(screen, &rect, SDL_MapRGB(screen->format,
						0x7F, 0x7F, 0x7F));
	rect.x++; rect.y++;
	rect.w -= 2; rect.h -= 2;
	SDL_FillRect(screen, &rect, 0x00000000);
	
	widget->cur_list_start_x = start_x + 8;
	widget->cur_list_start_y = start_y + 8;

	for (i = 0; i < widget->n_list_strings; i++) {
		draw_message_with_update(widget->list_strings[i], 
					 start_x + 8, start_y + 8,
					 widget->list_width, FALSE, FALSE);
		start_y += LINEHEIGHT + 1;
	}

	widget->cur_list_end_x = start_x + 8 + widget->list_width;
	widget->cur_list_end_y = start_y + 8;
}

/**
 * Show a widget
 *
 * @param[in]	widget			The widget
 */
void widget_show(Widget *widget)
{
	widget->visible = 1;
	put_image(widget->surface, widget->x, widget->y);

	if (widget->is_text)
		widget_draw_text(widget);

	if (widget->is_list && widget->is_showing_list)
		list_widget_draw_list(widget);

	if (!widget->enabled)
	{
		SDL_SetAlpha(disabler(), SDL_SRCALPHA, 128);
		if (!widget->is_text)
			put_image(disabler(), widget->x, widget->y);
		else
			put_image(disabler(), widget->x + widget->text_x - 4,
				  widget->y + widget->text_y);
	}
}

/**
 * Hide a widget
 *
 * @param[in]	widget			The widget
 */
void widget_hide(Widget *widget)
{
	widget->visible = 0;
}

/**
 * Enable or disable (grays out) a widget
 *
 * @param[in]	widget			The widget
 * @param[in]	enable			The status
 */
void widget_enable(Widget *widget, int enable)
{
	widget->enabled = enable;
}

/**
 * Destroy a widget
 *
 * @param[in]	widget			The widget
 */
void widget_destroy(Widget *widget)
{
	assert(widget);
	SDL_FreeSurface(widget->surface);
	widget_list = llist_remove(widget_list, widget);
	free(widget);
}

/**
 * Update the GUI: repaint all widgets on the screen.
 */
void update_gui(void)
{
	LList *rev, *cur;

	SDL_FillRect(screen, NULL, 0x00000000);

	/* First added widget drawn first. */
	rev = llist_reverse(widget_list);

	for (cur = rev; cur; cur = cur->next) {
		Widget *widget = (Widget *)cur->data;

		if (!widget->visible)
			continue;

		widget_show(widget);
	}
	llist_free(rev);
	SDL_UpdateRect(screen, 0, 0, 0, 0);
}

static int list_widget_selected(Widget *widget, int x, int y)
{
	int n_string;
	if (!widget->visible)
		return 0;
	if (!widget->enabled)
		return 0;
	if (!widget->is_list)
		return 0;
	if (!widget->is_showing_list)
		return 0;
	if (!is_in_box(x, y, widget->cur_list_start_x, widget->cur_list_start_y,
		widget->cur_list_end_x, widget->cur_list_end_y))
		return 0;
	n_string = (y - widget->cur_list_start_y) / (LINEHEIGHT + 1);
	
	if (n_string >= 0 && n_string < widget->n_list_strings
	    && widget->list_clicked_callback)
		widget->list_clicked_callback(widget, n_string,
				widget->list_strings[n_string]);
	return 1;
}

/**
 * Check whether a widget is clicked
 *
 * @param[in]	widget			The widget
 * @param[in]	x			The click X coord
 * @param[in]	y			The click Y coord
 *
 * @return 1 if the click is inside the widget, 0 otherwise
 */
static int widget_clicked(Widget *widget, int x, int y)
{
	return is_in_box(x, y,
		widget->x, widget->y,
		widget->x + widget->surface->w, widget->y + widget->surface->h);
}

/**
 * Get the currently editing (and visible) widget
 *
 * @return a widget, or NULL if no edition is occuring
 */
static Widget *gui_get_editing_widget(void)
{
	LList *cur;
	for (cur = widget_list; cur; cur = cur->next) {
		Widget *widget = (Widget *)cur->data;

		if (!widget->visible)
			continue;
		else if (widget->is_text && widget->is_editing)
			return widget;
	}
	return NULL;
}

/**
 * Get the currently listing (and visible) widget
 *
 * @return a widget, or NULL if no list is occuring
 */
static Widget *gui_get_listing_widget(void)
{
	LList *cur;
	for (cur = widget_list; cur; cur = cur->next) {
		Widget *widget = (Widget *)cur->data;

		if (!widget->visible)
			continue;
		else if (widget->is_list && widget->is_showing_list)
			return widget;
	}
	return NULL;
}

/**
 * Handle a keypress
 *
 * @param[in]	widget		The widget to update
 * @param[in]	key		The key code
 */
static void handle_input(Widget *widget, int key)
{
	int len;
	assert(widget->is_text);

	switch(key) {
	case SDLK_RETURN:
	case SDLK_KP_ENTER:
	case SDLK_ESCAPE:
		if (widget->text[0] == '\0') {
			const char *txt = widget->default_text;
			strcpy(widget->text, txt ? txt : "");
		}
		widget->is_editing = 0;
		if (widget->text_validate_callback)
		    widget->text_validate_callback(
			    widget, widget->text_validate_cb_data);
		break;
	case SDLK_BACKSPACE:
		if (widget->text[0] != '\0') {
			len = strlen(widget->text);
			widget->text[len - 1] = '\0';
		}
		break;
	default:
		len = strlen(widget->text);
		if (len < widget->max_text_len - 1) {
			widget->text[len] = key;
			len++;
		}
		break;
	}
}

static int handled(int type)
{
	if(type == SDL_USEREVENT_QUIT || type == SDL_QUIT ||
		type == SDL_KEYDOWN || type == SDL_MOUSEBUTTONDOWN ||
		type == SDL_USEREVENT_REFRESH)
		return TRUE;
	else
		return FALSE;
}
/**
 * Wait for an event to happen on the GUI
 *
 * @return 0 when a normal event arrived, -1 if the event that arrived should
 * make the caller quit the loop.
 */
int gui_wait_event(void)
{
	LList *cur;
	SDL_Event event;
	int x, y;
	Widget *widget;
	Widget *currently_editing;
	Widget *currently_listing;

	/* Not using event_poll here because we need more complicated things. */
	do {
		SDL_WaitEvent(&event);
	} while (!handled(event.type));

	if (event.type == SDL_USEREVENT_QUIT || event.type == SDL_QUIT)
		return -1;
	
	currently_editing = gui_get_editing_widget();
	currently_listing = gui_get_listing_widget();

	if (event.type == SDL_KEYDOWN) {
		SDL_KeyboardEvent *kevent = &event.key;
		if (handle_generic_event(&event) == -2)
			return -1;

		if (currently_editing && (kevent->keysym.unicode & 0xFF00) == 0)
			handle_input(currently_editing,
				kevent->keysym.unicode & 0xFF);
		return 0;
	}
	
	if (event.type != SDL_MOUSEBUTTONDOWN)
		return 0;

	if (currently_editing)
		handle_input(currently_editing, SDLK_RETURN);

	SDL_GetMouseState (&x, &y);

	widget = NULL;
	for (cur = widget_list; cur; cur = cur->next) {
		widget = (Widget *)cur->data;

		if (!widget->visible || !widget->enabled)
			continue;
		
		if (list_widget_selected(widget, x, y)) {
			widget->is_showing_list = 0;
			list_widget_free_strings(widget);
			break;
		}
		if (widget_clicked(widget, x, y)) {
			if (widget->clicked_callback)
				widget->clicked_callback(widget,
					widget->clicked_cb_data);
			else if (widget->is_text && widget->editable)
				widget->is_editing = 1;
			else if (widget->is_list) {
				widget->is_showing_list = !widget->is_showing_list;
				if (!widget->is_showing_list)
					list_widget_free_strings(widget);
			}
			break;
		}
	}

	if (currently_listing && widget && currently_listing != widget) {
		currently_listing->is_showing_list = 0;
		list_widget_free_strings(currently_listing);
	}

	return 0;
}

/**
 * Reload all images in the GUI. This is used when changing language.
 */
void gui_reload_images(void)
{
	LList *cur;
	for (cur = widget_list; cur; cur = cur->next) {
		Widget *widget = cur->data;
		widget_set_image(widget, NULL);
	}

}

int widget_get_width(Widget *widget)
{
	assert(widget);
	assert(widget->surface);
	return widget->surface->w;
}

int widget_get_height(Widget *widget)
{
	assert(widget);
	assert(widget->surface);
	return widget->surface->h;
}

int text_widget_get_x_offset(Widget *widget)
{
	assert(widget);
	assert(widget->is_text);
	return widget->text_x;
}

int text_widget_get_y_offset(Widget *widget)
{
	assert(widget);
	assert(widget->is_text);
	return widget->text_y;
}

/**
 * Init a list widget
 *
 * @param[in] widget
 * @param[in] get_strings	The callback called when the list has to be displayed.
 *				The returned string array will be freed
 * @param[in] list_clicked_callback
 *				The callback when a string in the list is clicked
 */
void list_widget_init(Widget *widget, int list_x, int list_y, int list_width,
		      int (*get_strings)(struct _Widget *widget, char ***strings),
		      void (*list_clicked_callback)(struct _Widget *widget, int index,
						    char *string))
{
	assert(widget->is_list == 0);
	widget->is_list = 1;
	
	widget->list_x = list_x;
	widget->list_y = list_y;
	widget->list_width = list_width;

	widget->get_strings = get_strings;
	widget->list_clicked_callback = list_clicked_callback;
	widget->is_showing_list = 0;
	widget->list_strings = NULL;
	widget->n_list_strings = 0;
}
