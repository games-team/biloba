/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdlib.h>
#include <string.h>
#include <SDL.h>
#include <SDL_mixer.h>
#include "sound.h"
#include "utils.h"

static int sound_inited = FALSE;

/**
 * Initialise SDL sound
 */
void sound_init(void)
{
	int rate = 22050;
	Uint16 format = AUDIO_S16;
	int channels = 2;
	int buffers = 4096;

	if (sound_inited)
		return;

	if(Mix_OpenAudio(rate, format, channels, buffers)) {
#ifdef DEBUG
		printf("can't open audio\n");
#endif
		return;
	}
	sound_inited = TRUE;
}

/**
 * De-initialise SDL sound
 */
void sound_stop(void)
{
	if (!sound_inited)
		return;
	Mix_CloseAudio();
	sound_inited = FALSE;
}

/**
 * Load a sound from disk
 *
 * @param[in] prefix	The directory to get the file from
 * @param[in] file	The file to load
 *
 * @return a Mix_Chunk pointer to the sound, or NULL if load failed.
 */
static Mix_Chunk *audio_load_prefix(const char *prefix, const char *file)
{
	Mix_Chunk *snd = NULL;
	char *path = malloc(strlen(prefix) + strlen(DIR_SEP) + strlen("snd")
			+ strlen(DIR_SEP) + strlen(file) +1);
	strcpy(path, prefix);
	strcat(path, DIR_SEP);
	strcat(path, "snd");
	strcat(path, DIR_SEP);
	strcat(path, file);

	snd = Mix_LoadWAV(path);

	free(path);

	return snd;
}

/**
 * Load a sound from disk
 *
 * @param[in] file	The file to load
 *
 * @return a Mix_Chunk pointer to the sound, or NULL if load failed.
 * @attention The file is looked for in PREFIX/res directory, then in
 * the executable's path if the first failed.
 */
static Mix_Chunk *sound_load(const char *file)
{
	Mix_Chunk *snd = NULL;

	if (!sound_inited) {
#ifdef DEBUG
		printf("not inited\n");
#endif
		return NULL;
	}

	snd = audio_load_prefix(PREFIX DIR_SEP "res", file);

	if (!snd)
		snd = audio_load_prefix(PREFIX, file);

	if (!snd && progpath != NULL)
		snd = audio_load_prefix(progpath, file);

	if (!snd) {
#ifdef DEBUG
		printf("can't load sound\n");
#endif
	}
	return snd;
}

static Mix_Chunk *s_type = NULL;
static Mix_Chunk *s_eat = NULL;

/**
 * Load all needed sounds
 */
void sound_load_sounds(void)
{
	s_type = sound_load("type.wav");
	s_eat  = sound_load("eat.wav");
}

/**
 * Play a Mix_Chunk
 *
 * @param[in] snd	The Mix_Chunk to play
 */
static void play(Mix_Chunk *snd)
{
	if (!sound_inited || !snd)
		return;
	Mix_PlayChannel(-1, snd, 0);
}

/**
 * Play a sound
 *
 * @param[in] sound	The sound ID to play
 */
void sound_play(SndId sound)
{
	if (!sound_inited)
		return;

	switch(sound) {
	case SND_TYPE:
		play(s_type);
		break;
	case SND_EAT:
		play(s_eat);
		break;
	default:
		break;
	}
}
