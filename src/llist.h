/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __LLIST_H__
#define __LLIST_H__

typedef struct _LList LList;

struct _LList {
	void *data;
	LList *next;
};

LList *llist_append(LList *list, void *data);
LList *llist_prepend(LList *list, void *data);
LList *llist_remove(LList *list, void *data);
LList *llist_copy(LList *list);
LList *llist_reverse(LList *list);
LList *llist_find(LList *list, void *data);
void   llist_free(LList *list);
int    llist_length(LList *list);

#define llist_for_each(list, func) {		\
	LList *tmplist = list;				\
	for (; tmplist; tmplist = tmplist->next) {	\
		func(tmplist->data);			\
	}						\
}

#endif 
