/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __TILE_H__
#define __TILE_H__

typedef enum {
	TILE_NO_TILE = 0,
	TILE_NORMAL,
	TILE_CENTER,
	TILE_UP_LEFT,
	TILE_UP_RIGHT,
	TILE_DOWN_LEFT,
	TILE_DOWN_RIGHT,
	NUM_TILE_TYPES
} TileType;
	
typedef struct _Tile Tile;

#include "pawn.h"

struct _Tile {
	TileType 	type;
	int		pos_x;
	int		pos_y;
	int		coord_x;
	int		coord_y;
	SDL_Surface	*surface;
	
	Pawn 		*pawn;
};

Tile *tile_get (int x, int y);
Tile *tile_get_by_coords (int x, int y);
Tile *tile_get_next_in_row(Tile *start, Tile *second);
int   tile_is_near_center(Tile *tile);
void  tile_draw (Tile *tile);
void  tile_draw_arrow (Tile *tile, Tile *to);
void  tile_highlight(Tile *tile, int toggle);
void tile_free_all(void);

LList *tile_get_accessible_surroundings (Tile *tile);
#endif
