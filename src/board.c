/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

/**
 * Biloba - Q1 2005
 * Game by Guillaume Demougeot <dmgt@wanadoo.fr>
 * Code by Colin Leroy <colin@colino.net>
 *
 * This file contains the board handling code (drawing
 * and placing pawns).
 */

#include <SDL.h>
#include <SDL_image.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"
#include "tile.h"
#include "pawn.h"
#include "player.h"
#include "options.h"
#include "font.h"
#include "msg.h"

static SDL_Surface *border = NULL;
static SDL_Surface *player = NULL;

static void board_show_help(void)
{
	static SDL_Surface *help = NULL;
#ifndef MAEMO
	if (help == NULL)
		help = biloba_load_image("commands.png");
#else
	if (help == NULL)
		help = biloba_load_image("commands-maemo.png");
#endif
	assert(help != NULL);
		
	put_image(help, HELP_X, HELP_Y);
}

/**
 * Build the playing board from scratch, and display it on screen
 */
void board_build(void)
{
	int x, y;
	int num = 0, nb_players = 0;

	for (x = 0; x < MAX_TILES_X; x++) {
		for (y = 0; y < MAX_TILES_Y; y++) {
			tile_get(x, y)->pawn = NULL;
			tile_draw(tile_get(x, y));
		}
	}

	for (nb_players = 0; nb_players < game_num_players(); nb_players++) {
		Player *p = NULL;
		for (num = 0; num < pawn_get_max(); num++) {
			pawn_draw(pawn_get(num, (PawnColor)nb_players, TRUE));
		}
		p = player_get((PawnColor)nb_players, TRUE, INPUT_LOCAL);
		player_set_name(p, options_get_player_name(nb_players));
		p->method = options_get_player_type(nb_players);
	}
	
	if (!border) {
#ifndef MAEMO
		border = biloba_load_image("border.png");
#else
		border = biloba_load_image("border-maemo.png");
#endif
		assert(border != NULL);
	}
	if (!player) {
		player = biloba_load_image("player.png");	
		assert(player != NULL);
	}
	put_image(border, X_OFFSET - 36, Y_OFFSET - 36);
	put_image(player, 5, YS - 65);

	board_show_help();	
	SDL_UpdateRect(screen, 0, 0, 0, 0);
}

/**
 * Destroy the board.
 */
void board_destroy(void)
{
	tile_free_all();
	pawn_free_all();
}

/**
 * Set the player turn
 *
 * @param[in] player		The player that should play
 * @param[in] display_your_turn	Whether to print "your turn"
 */
void board_set_player(Player *player, int display_your_turn)
{
	LList *pawns = pawn_get_all(player->color);
	Pawn *pawn;
	char *turn;
	int len;
	
	if (display_your_turn)
		len = strlen(get_msg(M_YOUR_TURN)) + strlen(player->name) + 1;
	else
		len = strlen(get_msg(M_ROUND)) + strlen(player->name) + 1;

	if (!pawns)
		return;

	turn = malloc(len);
	
	pawn = pawns->data;
	
	put_image(pawn->surface, 15, YS - 45);
	SDL_UpdateRect(screen, 15, YS - 45, 30, 30);

	if (display_your_turn)
		snprintf(turn, len, "%s%s", get_msg(M_YOUR_TURN), player->name);
	else
		snprintf(turn, len, "%s%s", get_msg(M_ROUND), player->name);
	
	clear_text(-1, G_MSG_X, G_MSG_Y);
	draw_message(turn, G_MSG_X, G_MSG_Y, -1, FALSE);
	
	free(turn);

	llist_free(pawns);
}

static int frozen = 0;

/**
 * Freeze GUI updates to the board
 */
void board_freeze(void) 
{
	frozen++;
}

/**
 * Thaw GUI updates to the board
 */
void board_thaw(void) {
	frozen--;
	if (frozen < 0)
		frozen = 0;
}

/**
 * Get whether the board GUI updates are frozen
 *
 * @return TRUE if it is, FALSE if not
 */
int board_frozen(void) {
	return frozen > 0 ? TRUE : FALSE;
}
