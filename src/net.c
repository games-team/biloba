/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

/**
 * Biloba - Q1 2005
 * Game by Guillaume Demougeot <dmgt@wanadoo.fr>
 * Code by Colin Leroy <colin@colino.net>
 *
 * This file contains all the network client code.
 */

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <time.h>
#ifndef __MINGW32__
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h> 
#include <netdb.h> 
#else
#include <winsock2.h>
#endif

#include <SDL.h>
#include <SDL_thread.h>
#include <SDL_mutex.h>
#include <string.h>

#include "utils.h"
#include "player.h"
#include "net.h"
#include "netops.h"
#include "font.h"
#include "game.h"
#include "msg.h"
#include "server.h"
#include "options.h"

#undef NET_DEBUG

static int net_magic_number = 0;

void net_init(void)
{
#ifdef __MINGW32__
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2,0),&wsaData);
#endif	
}

void net_stop(void)
{
#ifdef __MINGW32__
	WSACleanup();
#endif
}

#ifdef __MINGW32__
#define INVSOCK INVALID_SOCKET
#else
#define INVSOCK -1
#endif

static int game_running = FALSE;
static int main_sock = INVSOCK;
static int32_t mygameid = -1;
static int32_t mygameplayer = -1;

#ifdef __MINGW32__
void debug_err(int y, int err)
{
	switch(err) {
	case WSANOTINITIALISED:
		draw_message("WSANOTINITIALISED", 300, y, -1, FALSE);
		break;
	case WSAENETDOWN:
		draw_message("WSAENETDOWN", 300, y, -1, FALSE);
		break;
	case WSAEAFNOSUPPORT:
		draw_message("WSAEAFNOSUPPORT", 300, y, -1, FALSE);
		break;
	case WSAEINPROGRESS:
		draw_message("WSAEINPROGRESS", 300, y, -1, FALSE);
		break;
	case WSAEMFILE:
		draw_message("WSAEMFILE", 300, y, -1, FALSE);
		break;
	case WSAENOBUFS:
		draw_message("WSAENOBUFS", 300, y, -1, FALSE);
		break;
	case WSAEPROTONOSUPPORT:
		draw_message("WSAEPROTONOSUPPORT", 300, y, -1, FALSE);
		break;
	case WSAEPROTOTYPE:
		draw_message("WSAEPROTOTYPE", 300, y, -1, FALSE);
		break;
	case WSAESOCKTNOSUPPORT:
		draw_message("WSAESOCKTNOSUPPORT", 300, y, -1, FALSE);
		break;
	default:
		draw_message("UNKNOWN", 300, y, -1, FALSE);
	
	}
}
#endif

static char *server = NULL;

static const char *net_get_server(void)
{
	return server ? server:NET_HOST;
}

static int sock_connect(const char *hostname, unsigned short port)
{
	int sockfd = INVSOCK;
	struct hostent *server;
	struct sockaddr_in serv_addr;
	int err;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
#ifndef __MINGW32__
	if (sockfd < 0) {
#else
	if (sockfd == INVALID_SOCKET) {
#endif
#ifdef NET_DEBUG
		draw_message("socket error", 10, 10, -1, FALSE);
#ifdef __MINGW32__
		err = WSAGetLastError();
		debug_err(10, err);
#endif
		SDL_Delay(200);
#endif
		return INVSOCK;
	} else {
#ifdef NET_DEBUG
		draw_message("socket ok", 10, 10, -1, FALSE);
		SDL_Delay(200);
#endif
	}
	
	if (hostname == NULL)
		return INVSOCK;
	
	printf("\nConnecting to %s...\n", hostname);
	server = gethostbyname(hostname);
	if (server == NULL) {
#ifdef NET_DEBUG
		draw_message("gethostbyname error", 10, 42, -1, FALSE);
#ifdef __MINGW32__
		err = WSAGetLastError();
		debug_err(42, err);
#endif
		SDL_Delay(200);
#endif
		return INVSOCK;
	} else {
#ifdef NET_DEBUG
		draw_message("gethostbyname ok", 10, 42, -1, FALSE);
		SDL_Delay(200);
#endif
	}
	
	memset((char *) &serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	memcpy(&serv_addr.sin_addr, server->h_addr, 
		server->h_length);
	serv_addr.sin_port = htons(port);

	err = connect(sockfd, (struct sockaddr *)&serv_addr, 
		      sizeof(serv_addr));
#ifndef __MINGW32__	
	if (err < 0) {
#else
	if (err == SOCKET_ERROR) {
#endif
#ifdef NET_DEBUG
		draw_message("connect error", 10, 74, -1, FALSE);
#ifdef __MINGW32__
		err = WSAGetLastError();
		debug_err(74, err);
#endif
		SDL_Delay(200);
#endif
		printf("Connection error: %d\n", err);
		return INVSOCK;
	} else {
#ifdef NET_DEBUG
		draw_message("connect ok", 10, 74, -1, FALSE);
		SDL_Delay(200);
#endif
	}
	printf("Connected.\n");
	return (sockfd);
}

#define CHECK_MSG(s, str) {				\
	r = read_str(s, buf, 255);			\
	if (r <= 0 || strncmp(buf, str, strlen(str))) {	\
		printf("Unexpected reply \"%s\", "	\
			"expected \"%s\"\n",		\
			buf, str);			\
		goto err_protocol;			\
	}						\
}

int net_init_game(const char *gamename, int numplayers,
		  InputSystemMethod methods[], char *names[])
{
	char buf[255];
	int i = 0, n, r;
	assert(game_running == FALSE);
	
	SDL_FillRect(screen, NULL, 0x00000000);
	SDL_UpdateRect(screen, 0, 0, 0, 0);

	main_sock = sock_connect(net_get_server(), NET_PORT);
	if (main_sock == INVSOCK)
		return -1;

	printf("\n");

	/* init connection */
	r = send_str(main_sock, "NEWGAME");
	if (r < 0) {
		printf("Error sending NEWGAME: %s(%d)\n", net_err_str(r), r);
		goto err_protocol;
	}
	printf("> Sending game creation command\n");
	CHECK_MSG(main_sock, "OK");

	/* send name */
	assert(strlen(gamename) < 255);
	r = send_str(main_sock, gamename);
	if (r < 0) {
		printf("Error sending game name: %s(%d)\n", net_err_str(r), r);
		goto err_protocol;
	}
	printf("> Sending game name '%s'\n", gamename);

	/* get id */
	r = read_int(main_sock, &mygameid);
	if (r < 0) {
		printf("Error reading game ID: %s(%d)\n", net_err_str(r), r);
		goto err_protocol;
	}
	printf("< Received assigned game ID %d\n", mygameid);

	/* send nump, methods and names */
	buf[i] = (unsigned char)numplayers; i++;
	
	for (n = 0; n < numplayers; n++) {
		buf[i] = (unsigned char)methods[n];
		i++;
		strcpy(buf+i, names[n]);
		i+=strlen(names[n])+1;
		printf("> Sending player %d (%s) name '%s'\n", 
			n, input_system_names(methods[n]),
			names[n]);
	}
	r = send_msg(main_sock, buf, i);
	if (r < 0) {
		printf("Error sending player details: %s(%d)\n", net_err_str(r), r);
		goto err_protocol;
	}
	CHECK_MSG(main_sock, "READY");
	printf("< Received ack, now waiting for other players\n");

	mygameplayer = 0;
	return mygameid;
	
err_protocol:
	printf("Closing connection.\n");
	net_close();
	mygameid = -1;
	return -1;
}

static int talk_sock = INVSOCK;
static SDL_Thread *net_talk_thread = NULL;
static int talk_thread_run = 1;

static int msg_available_for_send = FALSE;
static char waiting_send_msg[256];
static int msg_available_for_recv = FALSE;
static char waiting_recv_msg[256];
static SDL_mutex *msg_mutex = NULL;

static int typing_msg = FALSE;
static char msg[MAX_MSG_LEN] = "";
static int msg_len = 0;

int net_is_typing_msg(void)
{
	return typing_msg;
}

static void net_talk_push_send_msg(const char *buf)
{
	if (net_talk_thread == NULL)
		return;

	SDL_LockMutex(msg_mutex);

	printf("> Sending message '%s'\n", buf);
	strncpy(waiting_send_msg, buf, sizeof(waiting_send_msg));
	waiting_send_msg[sizeof(waiting_send_msg) - 1] = '\0';

	msg_available_for_send = TRUE;
	
	SDL_UnlockMutex(msg_mutex);	
}

void net_set_typing_msg(int typing)
{
	if (!options_using_net())
		return;

	typing_msg = typing;
	if (!typing_msg && msg_len > 0) {
		/* Send pending message */
		net_talk_push_send_msg(msg);
	}
	printf("  %s typing message...\n", typing_msg ? "Started":"Stopped");
	/* Reset buffer */
	msg_len = 0;
	msg[msg_len] = '\0';

	if (typing) {
		char talk_str[256];
		clear_text(-1, G_TALK_X, G_TALK_Y);
		snprintf(talk_str, sizeof(talk_str), "%s: ",
			 options_get_player_name(mygameplayer));
		draw_message(talk_str, G_TALK_X, G_TALK_Y,
			     MAX_MSG_LEN * CHARWIDTH, TRUE);
	} else {
		clear_text(-1, G_TALK_X, G_TALK_Y);
	}
}

const char *net_get_pending_msg(void)
{
	if (!typing_msg)
		return NULL;
	return msg;
}

void net_type_msg(unsigned char c)
{
	char talk_str[MAX_MSG_LEN + 1];

	if (c == SDLK_ESCAPE) {
		msg_len = 0;
		net_set_typing_msg(FALSE);
		return;
	} else if (c == SDLK_BACKSPACE) {
		c='\0';
	} else if (c < 0x20 || c == 0x7f)
		return;

	assert(typing_msg == TRUE);

	if (msg_len >= MAX_MSG_LEN 
		- strlen(options_get_player_name(mygameplayer)) 
		- strlen(": ")
		- 1)
		return;

	if (c != '\0') {
		msg[msg_len] = c;
		msg_len++;
		msg[msg_len] = '\0';
	} else if (msg_len > 0) {
		msg_len--;
		msg[msg_len] = '\0';
	}


	snprintf(talk_str, sizeof(talk_str), "%s: %s",
		 options_get_player_name(mygameplayer), msg);

	clear_text(-1, G_TALK_X, G_TALK_Y);
	draw_message(talk_str, G_TALK_X, G_TALK_Y, MAX_MSG_LEN * CHARWIDTH, TRUE);
}

static void net_talk_push_recv_msg(const char *player, const char *msg)
{
	if (player == NULL || msg == NULL)

	SDL_LockMutex(msg_mutex);

	if (player != NULL && msg != NULL)
		snprintf(waiting_recv_msg, sizeof(waiting_recv_msg), "%s: %s", player, msg);
	else
		waiting_recv_msg[0] = '\0';

	waiting_recv_msg[sizeof(waiting_recv_msg) - 1] = '\0';

	msg_available_for_recv = TRUE;

	SDL_UnlockMutex(msg_mutex);
}

void net_pull_recv_msg(void)
{
	char *msg = NULL;

	SDL_LockMutex(msg_mutex);

	if (msg_available_for_recv) {
		msg_available_for_recv = FALSE;
		msg = strdup(waiting_recv_msg);
	}
	SDL_UnlockMutex(msg_mutex);

	if (msg) {
		printf("> Pulled message '%s'\n", msg);
		clear_text(-1, G_TALK_X, G_TALK_Y);
		draw_message(msg, G_TALK_X, G_TALK_Y, MAX_MSG_LEN * CHARWIDTH, FALSE);
		free(msg);
	}
}

int net_talk_thread_func(void *data)
{
	char buf[256];
	int r;
	time_t last_displayed_message = time(NULL);
	int should_clear = FALSE;

	if (talk_sock != INVSOCK) {
		sock_close(talk_sock);
		talk_sock = INVSOCK;
	}
	
	talk_sock = sock_connect(net_get_server(), NET_PORT);
	
	if (send_str(talk_sock, "TALK_MODE") < 0 ||
	    send_int(talk_sock, mygameid) < 0 ||
	    send_int(talk_sock, mygameplayer) < 0)
		goto err_protocol;

	CHECK_MSG(talk_sock, "OK");

	while (talk_thread_run) {
		Game *game;
		if (talk_sock == INVSOCK)
			break;

		SDL_LockMutex(msg_mutex);
		if (msg_available_for_send) {
			printf("< Message available\n");
			msg_available_for_send = FALSE;
			if (send_str(talk_sock, waiting_send_msg) < 0) {
				SDL_UnlockMutex(msg_mutex);
				printf("can't send message\n");
				break;
			}
			printf("> Sent message '%s'\n", waiting_send_msg);
		}
		SDL_UnlockMutex(msg_mutex);
		if (sock_select(talk_sock, 500) >= 0) {
			int32_t player;
			char buf[256];
			
			if (read_int(talk_sock, &player) < 0 ||
			    read_str(talk_sock, buf, 255) < 0) {
				break;
			}
			printf("< Got message from player %d: %s\n", player, buf);

			options_game_lock();
			game = options_get_game();
			if (!game) {
				options_game_unlock();
				printf("can't get game\n");
				break;
			}
			game_push_message(game, player, buf);
			options_game_unlock();
		}

		options_game_lock();
		game = options_get_game();
		if (!game) {
			options_game_unlock();
			printf("can't get game\n");
			break;
		}
		if (game->message_id > game->players_msg_id[mygameplayer]
                    && !net_is_typing_msg()) {
			/* Display first available message. */
			int msg_id = game->players_msg_id[mygameplayer] + 1;
			const Message *msg = &game->messages[msg_id];
			const char *player_name = game->player_name[msg->player];

			if (msg->player != mygameplayer) {
				last_displayed_message = time(NULL);
				should_clear = TRUE;
				net_talk_push_recv_msg(player_name, msg->message);
			}

			game->players_msg_id[mygameplayer]++;
		} else if (game->message_id == game->players_msg_id[mygameplayer]
			   && !net_is_typing_msg() && should_clear
			   && time(NULL) - last_displayed_message > 60) {
			net_talk_push_recv_msg(NULL, NULL);
			should_clear = FALSE;
		}
		options_game_unlock();
	}
	
err_protocol:
	sock_close(talk_sock);
	talk_sock = INVSOCK;
	return 0;
}

int net_open_talk_socket(void)
{
	if (net_talk_thread) {
		int tmp;
		talk_thread_run = 0;
		SDL_WaitThread(net_talk_thread, &tmp);
	}

	if (msg_mutex == NULL)
		msg_mutex = SDL_CreateMutex();

	talk_thread_run = 1;
	net_talk_thread = SDL_CreateThread(net_talk_thread_func, NULL);
	
	return 0;
}

int net_wait_ready(int num_players, char **player_names)
{
	int missing_players = 0, prev_missing_players = 0;
	char buf[255], player[255];
	int nplayer, i;
	
	if (main_sock == INVSOCK)
		return -1;

	do {
		if (event_poll() < 0)
			goto err_out;

		if (send_str(main_sock, "READY?") < 0)
			goto err_out;
		if (read_int(main_sock, &missing_players) < 0)
			goto err_out;
		sprintf(buf, get_msg(M_WAITING),
				missing_players, missing_players > 1 ? "s":"");
		SDL_FillRect(screen, NULL, 0x00000000);
		SDL_UpdateRect(screen, 0, 0, 0, 0);
		draw_message(buf, 150, 284, -1, FALSE);
		
		if (read_int(main_sock, &nplayer) < 0)
			goto err_out;

		if (read_str(main_sock, player, 255) < 0)
			goto err_out;

		if (prev_missing_players > missing_players) {
			Game *game;
			sprintf(buf, get_msg(M_JOINED),
					player);
			draw_message(buf, 150, 284 + 40, -1, FALSE);
			printf("< Received a new player join: %d '%s'\n",
				nplayer, player);

			/* Fixme the two player names overlap. */
			strcpy(player_names[nplayer], player);
			options_game_lock();
			game = options_get_game();
			if (game) {
				free(game->player_name[nplayer]);
				game->player_name[nplayer] = strdup(player);
			}
			options_game_unlock();
			if (delay_with_event_poll(1000) < 0)
				goto err_out;
		}
		
		prev_missing_players = missing_players;
		
		if (delay_with_event_poll(1000) < 0)
			goto err_out;
	} while (missing_players > 0);

	printf("\n");

	for (i = 0; i < num_players; i++) {
		if (read_str(main_sock, player, 255) < 0)
			return -1;
		strcpy(player_names[i], player);
	}

	return net_open_talk_socket();
err_out:
	net_close();
	return -1;
}

LList *net_get_games(int numplayer)
{
	char buf[255];
	int i = 0, numgames = 0, r;
	LList *games = NULL;
	assert(game_running == FALSE);

	/* We reconnect each time. Suboptimal. */
	if (main_sock != INVSOCK)
		net_close();

	main_sock = sock_connect(net_get_server(), NET_PORT);
	if (main_sock == INVSOCK)
		return NULL;

	printf("\n");

	/* init connection */
	r = send_str(main_sock, "LISTGAME");
	if (r < 0) {
		printf("Sending list games command failed: %s(%d)\n",
			net_err_str(r), r);
		goto err_protocol;
	}
	printf("> Asking for games list\n");
	CHECK_MSG(main_sock, "NUMP");
	
	/* send num players */
	r = send_int(main_sock, numplayer);
	if (r < 0) {
		printf("Sending number of players failed: %s(%d)\n",
			net_err_str(r), r);
		goto err_protocol;
	}
	printf("> Asking for games with %d players\n", numplayer);

	/* read number of games */
	r = read_int(main_sock, &numgames);
	if (r < 0) {
		printf("Getting number of games failed: %s(%d)\n",
			net_err_str(r), r);
		goto err_protocol;
	}
	printf("< Will receive %d available games\n", numgames);

	for (i = 0; i < numgames; i++) {
		Game *game = malloc(sizeof(Game));

		memset(game, 0, sizeof(Game));

		game->num_players = numplayer;

		r = read_int(main_sock, &game->id);
		if (r < 0) {
			printf("Getting game ID failed: %s(%d)\n",
				net_err_str(r), r);
			free(game);
			goto err_protocol;
		}

		r = read_str(main_sock, buf, 255);
		if (r < 0) {
			printf("Getting game name failed: %s(%d)\n",
				net_err_str(r), r);
			free(game);
			goto err_protocol;
		}
		game->name = strdup(buf);
		printf("< Received game ID %d, '%s'\n", game->id, game->name);
		games = llist_append(games, game);
	}
	
	return games;

err_protocol:
	net_close();
	return NULL;
}

int net_get_info(Game *game)
{
	char buf[255];
	int i, r;

	if (main_sock == INVSOCK)
		return -1;

	printf("\n");

	r = send_str(main_sock, "PRELJOIN");
	if (r < 0) {
		printf("Sending preliminary join command failed: %s(%d)\n",
			net_err_str(r), r);
		goto err_protocol;
	}

	r = send_int(main_sock, game->id);
	if (r < 0) {
		printf("Sending game ID failed: %s(%d)\n", net_err_str(r), r);
		goto err_protocol;
	}
	printf("> Asking for game ID %d details\n", game->id);

	memset(buf, 0, 255);
	r = read_str(main_sock, buf, 255);
	if (r < 0) {
		printf("Getting anwser failed: %s(%d)\n", net_err_str(r), r);
		goto err_protocol;
	}

	if (!strncmp(buf, "ERROR1", 6)) {
		printf("Error getting information (game %d doesn't exist)\n",
			game->id);
		goto err_protocol;
	}

	for (i = 0; i < game->num_players; i++) {
		r = read_str(main_sock, buf, 255);
		if (r < 0) {
			printf("Failed getting player %d's name: %s(%d)\n", i,
				net_err_str(r), r);
			goto err_protocol;
		}
		printf("< Received player %d's name '%s')\n", i, buf);
		game->player_name[i] = strdup(buf);
		game->player_type[i] = INPUT_NETWORK;
	}
	
	r = read_int(main_sock, &game->first_avail_spot);
	if (r < 0) {
		printf("Getting first available spot failed: %s(%d)\n",
			net_err_str(r), r);
		goto err_protocol;
	}
	printf("< Received first available spot %d)\n", game->first_avail_spot);
	
	return 0;

err_protocol:
	net_close();
	return -1;
}

int net_join(int id, int nump, const char *my_player_name)
{
	char buf[255];
	int r;

	if (main_sock == INVSOCK)
		return -1;

	printf("\n");

	r = send_str(main_sock, "JOIN");
	if (r < 0) {
		printf("Sending join command failed: %s(%d)\n", net_err_str(r), r);
		goto err_protocol;
	}
	printf("> Sending join command\n");
	r = send_int(main_sock, nump);
	if (r < 0) {
		printf("Sending player number failed: %s(%d)\n", net_err_str(r), r);
		goto err_protocol;
	}
	printf("> Sending my player number %d\n", nump);

	r = read_str(main_sock, buf, 255);
	if (r < 0) {
		printf("Reading join reply failed: %s(%d)\n", net_err_str(r), r);
		goto err_protocol;
	}

	if (strncmp(buf, "OK", 2)) {
		printf("Join refused.\n");
		goto err_protocol;
	}
	
	r = send_str(main_sock, my_player_name);
	if (r < 0) {
		printf("Sending player name failed: %s(%d)\n", net_err_str(r), r);
		goto err_protocol;
	}
	printf("> Sending my player name '%s'\n", my_player_name);

	printf("\n");

	mygameid = id;
	mygameplayer = nump;

	return 0;

err_protocol:
	net_close();
	return -1;
}

void net_send_event(int player, int x, int y)
{
	int r;

	if(main_sock == INVSOCK)
		return;

	if ((r = send_char(main_sock, 'p')) < 0 ||
	    (r = send_int(main_sock, player)) < 0 ||
	    (r = send_int(main_sock, x)) < 0 ||
	    (r = send_int(main_sock, y)) < 0) {
		printf("> Error sending event: %s (%d)\n", net_err_str(r), r);
		net_close();
		return;
	}

	printf("> Sending player %d event x %d, y %d\n", player, x, y);
}

static SDL_Thread *net_event_thread = NULL;
static SDL_sem *net_event_thread_sem = NULL;
static SDL_mutex *net_event_thread_mutex = NULL;

typedef struct {
	int player;
	int x;
	int y;
} NetData;

static int net_event_thread_func(void *data)
{
	NetData *vals = data;
	int r;
	int32_t x = -1, y = -1;

	if(main_sock == INVSOCK) {
		vals->x = -2;
		vals->y = -2;
		SDL_SemPost(net_event_thread_sem);
		return -1;
	}
again:
	printf("< Asking for player event\n");
	if ((r = send_char(main_sock, 'r')) < 0 ||
	    (r = send_int(main_sock, vals->player)) < 0 ||
	    (r = read_int(main_sock, &x)) < 0 ||
	    (r = read_int(main_sock, &y)) < 0) {
		SDL_LockMutex(net_event_thread_mutex);
		vals->x = -2;
		vals->y = -2;
		printf("< Error getting player %d event: %s(%d)\n",
			vals->player, net_err_str(r), r);
		SDL_UnlockMutex(net_event_thread_mutex);
		SDL_SemPost(net_event_thread_sem);
		return -1;
	} else {
		if (x == -1 && y == -1)
			goto again;
		SDL_LockMutex(net_event_thread_mutex);
		vals->x = x;
		vals->y = y;
		printf("> Got player %d event x %d, y %d\n", vals->player,
			vals->x, vals->y);
		SDL_UnlockMutex(net_event_thread_mutex);
		SDL_SemPost(net_event_thread_sem);
	}

	return 0;
}

void net_get_event(int player, int *x, int *y)
{
	NetData data;
	int tmp;

	data.player = player;
	data.x = *x = -1;
	data.y = *y = -1;

	net_event_thread_sem = SDL_CreateSemaphore(0);
	net_event_thread_mutex = SDL_CreateMutex();
	net_event_thread = SDL_CreateThread(net_event_thread_func, &data);

	SDL_LockMutex(net_event_thread_mutex);
	while (data.x == -1 && data.y == -1) {
		SDL_UnlockMutex(net_event_thread_mutex);
		SDL_UpdateRect(screen, 0, 0, 0, 0);
		SDL_SemWaitTimeout(net_event_thread_sem, 50);
		if (event_poll() < 0) {
			SDL_KillThread(net_event_thread);
			net_event_thread = NULL;
			data.x = -2;
			data.y = -2;
			net_close();
			break;
		}
		SDL_LockMutex(net_event_thread_mutex);
	}
	SDL_UnlockMutex(net_event_thread_mutex);

	if (net_event_thread)
		SDL_WaitThread(net_event_thread, &tmp);
	SDL_DestroySemaphore(net_event_thread_sem);
	SDL_DestroyMutex(net_event_thread_mutex);

	*x = data.x;
	*y = data.y;
}

void net_end_game(int player)
{
	int r;

	if(main_sock == INVSOCK)
		return;

	if ((r = send_char(main_sock, 'q')) < 0 ||
	    (r = send_int(main_sock, player)) < 0) {
		printf("> Error sending quit event: %s (%d)\n", net_err_str(r), r);
		net_close();
	}

	printf("> Sending quit event for player %d\n", player);
}

void net_close(void)
{
	if(main_sock == INVSOCK)
		return;

	sock_close(main_sock);
	main_sock = INVSOCK;
}

static SDL_Thread *server_thread = NULL;

static int server_main_thread(void *data)
{
	server_set_select_interval(1);
	server_set_do_broadcast(1, net_magic_number);
	return server_start(NULL, NULL);
}

void net_set_server(const char *srv)
{
	int was_local;

	was_local = server && !strcmp(server, "127.0.0.1");

	free(server);
	server = srv ? strdup(srv):NULL ;
	
	if (server && !strcmp(server, "127.0.0.1")) {
		if (server_thread == NULL) {
			net_magic_number = rand();
			server_thread = SDL_CreateThread(server_main_thread, NULL);
		}
	} else if (was_local) {
		int res;
		server_stop();
		SDL_WaitThread(server_thread, &res);
		net_magic_number = 0;
		server_thread = NULL;
	}
}

static SDL_mutex *server_scan_mutex = NULL;
static SDL_Thread *server_scan_thread = NULL;
static int server_scan_running = 0;
static int n_servers_found = 0;
static char **server_found = NULL;

static int server_is_ok(const char *srv, char **servers, int n_servers)
{
	int i;

	if (!srv)
		return FALSE;

	if (!strcmp(srv, "0.0.0.0"))
		return FALSE;

	for (i = 0; i < n_servers; i++)
		if (!strcmp(srv, servers[i]))
			return FALSE;
	
	return TRUE;
}

static int scan_for_servers(void *data)
{
	int sock = 0, i;
	struct sockaddr_in addr;
	char mybuf[512];
#ifndef __MINGW32__
	socklen_t sock_len;
	static int optOn = 1;
#else
	int sock_len;
	static const char optOn = 1;
#endif
	int ret;
	static int first_run = 1;

	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0)
		return 0;

	memset((char *) &addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = htons(BCAST_PORT);
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR,
			&optOn, sizeof(optOn));
	setsockopt(sock, SOL_SOCKET, SO_BROADCAST,
			&optOn, sizeof(optOn));
	if (bind(sock, (struct sockaddr *) &addr, sizeof(addr)) < 0)
		return 0;
	
	while (server_scan_running) {
		char own_id[BCAST_LEN];

		get_bcast_msg(own_id, BCAST_LEN, net_magic_number);

		if (sock_select(sock, first_run ? 1100:100) == -1) {
			if (!first_run) {
				SDL_Event user_event;
				user_event.type=SDL_USEREVENT_REFRESH;
				user_event.user.code=2;
				user_event.user.data1=NULL;
				user_event.user.data2=NULL;
				SDL_PushEvent(&user_event);
				sleep(2);
			}
			SDL_LockMutex(server_scan_mutex);

			for (i = 0; i < n_servers_found; i++) {
				free(server_found[i]);
			}
			free(server_found);
			server_found = NULL;
			n_servers_found = 0;

			SDL_UnlockMutex(server_scan_mutex);
			first_run = 1;
			continue;
		}
		first_run = 0;

		memset(mybuf, 0, sizeof(mybuf));
		sock_len = sizeof(addr);
		ret = recvfrom(sock, mybuf, sizeof(mybuf), 0,
					(struct sockaddr *)&addr,
					&sock_len);
		if (ret < 0) {
			ret = get_net_error();
			printf("error %s (%d)\n", net_err_str(ret), ret);
		}
 
		if (ret == BCAST_LEN && !strncmp(mybuf, "Biloba", 6)
		 && strcmp(mybuf, own_id)) {
			const char *hostname = get_hostname(&addr);

			SDL_LockMutex(server_scan_mutex);

			if (server_is_ok(hostname,
					  server_found, n_servers_found)) {
				n_servers_found++;
				server_found = realloc(server_found,
					n_servers_found * sizeof(char *));
				server_found[n_servers_found - 1] =
					strdup(hostname);
			}
			SDL_UnlockMutex(server_scan_mutex);
		}
	}
	sock_close(sock);
	return 0;
}

int net_get_servers(char ***distant_servers)
{
	int r, i;

	if (server_scan_thread == NULL) {
		server_scan_mutex = SDL_CreateMutex();
		server_scan_running = 1;
		server_scan_thread = SDL_CreateThread(
			scan_for_servers, NULL);
	}
	
	(*distant_servers) = NULL;

	SDL_LockMutex(server_scan_mutex);

	r = n_servers_found;

	(*distant_servers) = malloc(r * sizeof(char *));

	for (i = 0; i < r; i++)
		(*distant_servers)[i] = strdup(server_found[i]);

	SDL_UnlockMutex(server_scan_mutex);
	
	return r;
}

void net_stop_getting_servers(void)
{
	if (server_scan_thread != NULL) {
		server_scan_running = 0;
		SDL_KillThread(server_scan_thread);
		SDL_DestroyMutex(server_scan_mutex);
	}
	server_scan_thread = NULL;
	server_scan_mutex = NULL;
}
