/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __NET_H__
#define __NET_H__

#include "utils.h"
#include "player.h"
#include "llist.h"
#include "game.h"

void net_init(void);
void net_stop(void);
int net_init_game(const char *gamename, int numplayers,
		  InputSystemMethod methods[], char *names[]);
int net_wait_ready(int num_players, char **player_names);
LList *net_get_games(int numplayer);
int net_get_info(Game *game);
int net_join(int id, int nump, const char *my_player_name);
void net_send_event(int player, int x, int y);
void net_get_event(int player, int *x, int *y);
void net_end_game(int player);
void net_close(void);
void net_set_server(const char *srv);
int net_get_servers(char ***distant_servers);
void net_stop_getting_servers(void);

void net_pull_recv_msg(void);

int net_is_typing_msg(void);
void net_set_typing_msg(int typing);
const char *net_get_pending_msg(void);
void net_type_msg(unsigned char c);

#endif
