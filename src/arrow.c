/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

/**
 * Biloba - Q1 2005
 * Game by Guillaume Demougeot <dmgt@wanadoo.fr>
 * Code by Colin Leroy <colin@colino.net>
 *
 * This file contains the little arrow-related functions.
 */

#include <stdlib.h>
#include <SDL.h>
#include <SDL_image.h>

#include "utils.h"
#include "arrow.h"

static LList *my_arrows = NULL;

/**
 * Find an arrow of a given type. 
 *
 * @param[in] type	The type of the arrow to find
 *
 * @return an arrow of the correct type, or NULL if not found.
 */
static Arrow *arrow_find(ArrowType type)
{
	LList *cur = my_arrows;
	
	while(cur) {
		Arrow *a = (Arrow *)cur->data;
		if (a->type == type)
			return a;
		cur = cur->next;
	}
	return NULL;
}

static SDL_Surface *arrow_models[NUM_ARROWS] = {NULL};

/**
 * Initialise arrow models from the images
 */
static void init_arrow_models(void)
{
	int i = 0;
	assert(arrow_models[ARROW_UP] == NULL);
	
	arrow_models[ARROW_UP] = biloba_load_image("arrow-up.png");
	arrow_models[ARROW_DOWN] = biloba_load_image("arrow-down.png");
	arrow_models[ARROW_LEFT] = biloba_load_image("arrow-left.png");
	arrow_models[ARROW_RIGHT] = biloba_load_image("arrow-right.png");
	arrow_models[ARROW_UP_LEFT] = biloba_load_image("arrow-up-left.png");
	arrow_models[ARROW_UP_RIGHT] = biloba_load_image("arrow-up-right.png");
	arrow_models[ARROW_DOWN_LEFT] = biloba_load_image("arrow-down-left.png");
	arrow_models[ARROW_DOWN_RIGHT] = biloba_load_image("arrow-down-right.png");
	
	for (i = 0; i < NUM_ARROWS; i++)
		assert(arrow_models[i] != NULL);
}

/**
 * Get an arrow of a given type. 
 *
 * @param[in] type	The type of the arrow to get
 *
 * @return an arrow of the correct type.
 */
static Arrow *arrow_get (ArrowType type)
{
	Arrow *new_arrow = arrow_find(type);
	if (new_arrow != NULL)
		return new_arrow;
	
	if (arrow_models[ARROW_UP] == NULL)
		init_arrow_models();
	
	new_arrow = malloc(sizeof(Arrow));
	
	new_arrow->type = type;
	new_arrow->surface = arrow_models[new_arrow->type];
	
	my_arrows = llist_append(my_arrows, new_arrow);

	return new_arrow;
}

/**
 * Draw an arrow of a given type. 
 *
 * @param[in] type	The type of the arrow to get
 * @param[in] x		The X coordinate (pixels)
 * @param[in] y		The Y coordinate (pixels)
 *
 * @return an arrow of the correct type.
 */
void arrow_draw(ArrowType type, int x, int y)
{
	Arrow *arrow = arrow_get(type);
	put_image(arrow->surface, x, y);
	if (is_playing())
		SDL_UpdateRect(screen, x, y, 20, 20);
}

/**
 * Draw all arrows for re-placing pawns that just ate one.
 *
 * @param[in] player	The player for which we want to draw arrows
 */
void arrow_draw_all(Player *player)
{
	LList *pawns = pawn_get_replacement_pending_pawns(player->color);
	LList *cur = pawns;

	while (cur) {
		Pawn *pawn = (Pawn *)cur->data;
		LList *to_tiles = pawn->just_ate_on;
		while(to_tiles) {
			Tile *to_tile = (Tile *)to_tiles->data;
			tile_draw_arrow(pawn_get_tile(pawn), to_tile);
			to_tiles = to_tiles->next;
		}
		cur = cur->next;
	}
	llist_free(pawns);
	SDL_UpdateRect(screen, 0, 0, 0, 0);
}
