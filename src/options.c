/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

/**
 * Biloba - Q1 2005
 * Game by Guillaume Demougeot <dmgt@wanadoo.fr>
 * Code by Colin Leroy <colin@colino.net>
 *
 * This file contains the options code used at startup.
 */

#include <stdlib.h>
#include <SDL.h>
#include <SDL_image.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <limits.h>
#include <libgen.h>

#include "options.h"
#include "utils.h"
#include "arrow.h"
#include "font.h"
#include "player.h"
#include "net.h"
#include "game.h"
#include "help.h"
#include "msg.h"
#include "widget.h"
#include "replay.h"

#define BILOBA_MIN_X 		 (300)
#define NUM_PLAYERS_MIN_X 	 (100)
#define PLAYERS_MIN_X 		 (50)
#define GAMENAME_MIN_X 		 (50)
#define SERVER_MIN_X 		 (50)
#define CHOOSE_SERVER_MIN_X 	 (410)
#define CREATEGAME_MIN_X 	 (50)
#define JOINGAME_MIN_X 		 (300)
#define START_MIN_X 		 (550)
#define HELP_MIN_X 		 (720)
#define LANG_MIN_X 		 (688)
#define PLAYERPAWN_MIN_X(i) 	 (100)
#define PLAYERNAME_MIN_X(i) 	 (140)
#define PLAYERTYPE_MIN_X(i) 	 (340)
#define NETWORK_GAMES_LIST_MIN_X (550)
#define GAMELIST_MIN_X(i) 	 (550)
#define SAVE_LAST_MIN_X 	 (550)
#define SAVED_MIN_X		 (100)

#ifndef MAEMO
#define BILOBA_MIN_Y		 (20)
#define PLAYERS_MIN_Y		 (100)
#define GAMENAME_MIN_Y		 (400)
#define SERVER_MIN_Y		 (350)
#define CHOOSE_SERVER_MIN_Y	 (350)
#define CREATEGAME_MIN_Y	 (500)
#define JOINGAME_MIN_Y		 (500)
#define START_MIN_Y		 (500)
#define HELP_MIN_Y		 (40)
#define LANG_MIN_Y		 (40)
#define PLAYERPAWN_MIN_Y(i)	 (200 + (40*(i-2)))
#define PLAYERNAME_MIN_Y(i)	 (200 + (40*(i-2)))
#define PLAYERTYPE_MIN_Y(i)	 (200 + (40*(i-2)))
#define NETWORK_GAMES_LIST_MIN_Y (100)
#define GAMELIST_MIN_Y(i)	 (151 + (40*(i)))
#define SAVE_LAST_MIN_Y		 (400)
#define SAVED_MIN_Y		 (275)
#else
#define BILOBA_MIN_Y		 (10)
#define PLAYERS_MIN_Y		 (70)
#define GAMENAME_MIN_Y		 (367)
#define SERVER_MIN_Y		 (320)
#define CHOOSE_SERVER_MIN_Y	 (320)
#define CREATEGAME_MIN_Y	 (420)
#define JOINGAME_MIN_Y		 (420)
#define START_MIN_Y		 (420)
#define HELP_MIN_Y		 (30)
#define LANG_MIN_Y		 (30)
#define PLAYERPAWN_MIN_Y(i)	 (170 + (40*(i-2)))
#define PLAYERNAME_MIN_Y(i)	 (170 + (40*(i-2)))
#define PLAYERTYPE_MIN_Y(i)	 (170 + (40*(i-2)))
#define NETWORK_GAMES_LIST_MIN_Y (70)
#define GAMELIST_MIN_Y(i)	 (121 + (40*(i)))
#define SAVE_LAST_MIN_Y		 (367)
#define SAVED_MIN_Y		 (215)
#endif

/* GUI widgets */
static Widget *numplayers = NULL;
static Widget *numplayers_icon = NULL;
static Widget *title = NULL;
static Widget *player_pawn_box[4] = { NULL, NULL, NULL, NULL };
static Widget *player_name[4] = { NULL, NULL, NULL, NULL };
static Widget *player_pawn[4] = { NULL, NULL, NULL, NULL };
static Widget *player_type[4] = { NULL, NULL, NULL, NULL };
static Widget *server = NULL;
static Widget *server_chooser = NULL;
static Widget *game_name = NULL;
static Widget *network_games = NULL;
static Widget *network_game[6] = { NULL, NULL, NULL, NULL, NULL, NULL };
static Widget *create_game = NULL;
static Widget *join_game = NULL;
static Widget *start = NULL;
static Widget *help = NULL;
static Widget *lang = NULL;
static Widget *save_last = NULL;
static Widget *saved = NULL;

/* Options states */
static int num_players = 2;
static InputSystemMethod player_types[4] = { INPUT_LOCAL, INPUT_LOCAL, INPUT_LOCAL, INPUT_LOCAL };
static int network_create_enabled = 0;
static int network_join_enabled = 0;
LList *network_games_list = NULL;
int selected_net_game_id = -1;
int selected_net_game_player_slot = -1;
static int start_clicked = FALSE;
char *player_names[4] = { NULL, NULL, NULL, NULL };

/* Return the name of the image for a player type */
static const char *get_input_type_img(InputSystemMethod input)
{
	switch(input)
	{
	case INPUT_LOCAL: 	return "local.png";
	case INPUT_NETWORK:	return "reseau.png";
	case INPUT_AI: 		return "computer.png";
	default:		assert(0);
	}

	return "";
}

static Game *cur_game = NULL;
static SDL_mutex *cur_game_mutex = NULL;

Game *options_get_game(void)
{
	return cur_game;
}

void options_game_lock(void)
{
	SDL_LockMutex(cur_game_mutex);
}

void options_game_unlock(void)
{
	SDL_UnlockMutex(cur_game_mutex);
}
/**
 * Get the player number's from a widget
 *
 * @param[in]	widget		The widget
 *
 * @return a number between 0 and 3 if the widget is a player type or name,
 * -1 otherwise.
 */
static int get_player_num_from_widget(Widget *widget)
{
	int i;
	
	for (i = 0; i < 4; i++)
		if (widget == player_type[i] || widget == player_name[i])
			return i;
	
	return -1;
}

/**
 * Copy the (player name's) widget text to the internal player names table
 * that is used for network games exchanging of names.
 *
 * @param[in] 	widget		The player name's widget
 * @param[in]	data		The (unused) callback data
 */
static void player_name_copy(Widget *widget, void *data)
{
	int num = get_player_num_from_widget(widget);

	if (num < 0)
		return;

    	strncpy(player_names[num], text_widget_get_text(widget), PLAYER_NAME_LEN);
}

/**
 * Set the type of a player (network, AI or local), and update GUI parts
 * accordingly.
 *
 * @param[in] 	num		The player number
 * @param[in]	type		The input type
 */
static void player_set_type(int num, InputSystemMethod type)
{
	player_types[num] = type;
	widget_set_image(player_type[num], get_input_type_img(player_types[num]));
	text_widget_set_editable(player_name[num], type != INPUT_NETWORK);
	widget_enable(player_name[num], type != INPUT_NETWORK);
	widget_enable(player_type[num], type != INPUT_NETWORK);
	if (type != INPUT_NETWORK &&
	    !strcmp(text_widget_get_text(player_name[num]), "")) {
		text_widget_set_text(player_name[num], NULL);
		player_name_copy(player_name[num], NULL);
	}
}

/**
 * Select a network game
 *
 * @param[in] 	widget		The network game's widget
 * @param[in]	data		The callback data, a Game * structure
 */
static void select_net_game(Widget *widget, void *data)
{
	Game *game = data;
	int i;

	assert(game != NULL);

	if (net_get_info(game) < 0)
		return;

	text_widget_set_text(game_name, game->name);

	selected_net_game_id = game->id;

	for (i = 0; i < game->num_players; i++) {
		if (i + 1 == game->first_avail_spot) {
			player_set_type(i, INPUT_LOCAL);
			widget_enable(player_type[i], 0);
			selected_net_game_player_slot = i;
		} else {
			text_widget_set_text(player_name[i],
					     game->player_name[i]);
			player_set_type(i, INPUT_NETWORK);
		}
	}
}

/**
 * Update all player types according to the internal state (network game
 * creation or join planned).
 */
static void update_player_types(void)
{
	int i;
	if (network_create_enabled) {
		player_set_type(0, INPUT_LOCAL);
		widget_enable(player_type[0], 0);
		for (i = 1; i < num_players; i++)
			player_set_type(i, INPUT_NETWORK);
	} else if (!network_join_enabled) {
		widget_enable(player_type[0], 1);
		for (i = 0; i < num_players; i++)
			player_set_type(i, INPUT_LOCAL);
	} else if (network_join_enabled) {
		/* handled by select_net_game */
	}
}

/**
 * Update the network games list according to the internal state (is joining
 * selected, number of players, previously selected network game).
 */
static void update_games_list(void)
{
	LList *cur;
	int i;

	llist_for_each(network_games_list, free_game);
	llist_free(network_games_list);
	network_games_list = NULL;

	if (network_join_enabled) {
		network_games_list = net_get_games(num_players);
		widget_show(network_games);
	} else
		widget_hide(network_games);
	
	if (selected_net_game_id > 0 && network_join_enabled) {
		network_join_enabled = 0;
		selected_net_game_id = -1;
		text_widget_set_text(game_name, "");
		update_player_types();
		network_join_enabled = 1;
	}

	cur = network_games_list;
	for (i = 0; i < 6; i++) {
		Game *game;
		if (cur) {
			game = cur->data;
			cur = cur->next;
		} else 
			game = NULL;
		
		if (network_join_enabled)
			widget_show(network_game[i]);
		else
			widget_hide(network_game[i]);

		if (network_join_enabled && game != NULL) {
			widget_set_clicked_callback(network_game[i],
					    select_net_game, game);
			text_widget_set_text(network_game[i], game->name);
		} else {
			widget_set_clicked_callback(network_game[i],
					    NULL, NULL);
			text_widget_set_text(network_game[i], "");
		}
	}
}

static void set_server_from_string(const char *string)
{
	if (string && !strcmp(string, get_msg(M_OFFICIAL_SERVER)))
		net_set_server(NULL);
	else if (string && !strcmp(string, get_msg(M_LOCAL_SERVER)))
		net_set_server("127.0.0.1");
	else if (string)
		net_set_server(string);
	else
		net_set_server(NULL);
}

/**
 * Update the network server to use.
 *
 * @param[in]	widget		The server's widget
 * @param[in]	data		Unused callback data
 */
static void set_server(Widget *widget, void *data)
{
	const char *text = text_widget_get_text(widget);

	set_server_from_string(text);
	update_games_list();
}

/**
 * Update the number of players.
 *
 * @param[in]	widget		The number of players' widget
 * @param[in]	data		Unused callback data
 */
static void num_players_clicked(Widget *widget, void *data)
{
	int i;
	if (num_players < 4)
		num_players++;
	else
		num_players = 2;

	if (num_players == 2)
		widget_set_image(numplayers_icon, "2play.png");
	else if (num_players == 3)
		widget_set_image(numplayers_icon, "3play.png");
	else if (num_players == 4)
		widget_set_image(numplayers_icon, "4play.png");

	for (i = 0; i < 4; i++) {
		if (i < num_players) {
			widget_show(player_pawn_box[i]);
			widget_show(player_pawn[i]);
			widget_show(player_name[i]);
			widget_show(player_type[i]);
		} else {
			widget_hide(player_pawn_box[i]);
			widget_hide(player_pawn[i]);
			widget_hide(player_name[i]);
			widget_hide(player_type[i]);
		}
	}
	update_player_types();
	update_games_list();
}

/**
 * Update the player input method.
 *
 * @param[in]	widget		The player's input method widget
 * @param[in]	data		Unused callback data
 */
static void update_player_method(Widget *widget, void *data)
{
	int num = get_player_num_from_widget(widget);

	if (num < 0)
		return;

	if (player_types[num] == INPUT_LOCAL)
		player_set_type(num, INPUT_AI);
	else
		player_set_type(num, INPUT_LOCAL);
}

/**
 * Update the GUI after clicking the Create Game button.
 *
 * @param[in]	widget		The Create Game widget
 * @param[in]	data		Unused callback data
 */
static void do_create_game(Widget *widget, void *data)
{
	network_create_enabled = !network_create_enabled;
	network_join_enabled = 0;
	if (network_create_enabled) {
		text_widget_set_text(game_name, NULL);
		widget_show(game_name);
		widget_enable(game_name, 1);
		text_widget_set_editable(game_name, 1);
		widget_show(server);
		widget_show(server_chooser);
		widget_enable(server, 1);
		text_widget_set_editable(server, 1);
	} else {
		widget_hide(game_name);
		widget_hide(server);
		widget_hide(server_chooser);
	}

	update_player_types();
}

/**
 * Update the GUI after clicking the Join Game button.
 *
 * @param[in]	widget		The Join Game widget
 * @param[in]	data		Unused callback data
 */
static void do_join_game(Widget *widget, void *data)
{
	network_join_enabled = !network_join_enabled;
	network_create_enabled = 0;
	if (network_join_enabled) {
		text_widget_set_text(game_name, "");
		widget_show(game_name);
		widget_enable(game_name, 0);
		text_widget_set_editable(game_name, 0);
		widget_show(server);
		widget_show(server_chooser);
		widget_enable(server, 1);
		text_widget_set_editable(server, 1);
	} else {
		widget_hide(game_name);
		widget_hide(server);
		widget_hide(server_chooser);
	}
	update_games_list();
	update_player_types();
}

/**
 * Signal the GUI after clicking the Play! button.
 *
 * @param[in]	widget		The Play widget
 * @param[in]	data		Unused callback data
 */
static void do_start(Widget *widget, void *data)
{
	start_clicked = 1;
}

/**
 * Signal the GUI to save the last played game.
 *
 * @param[in]	widget		The Play widget
 * @param[in]	data		Unused callback data
 */
static void do_save_last(Widget *widget, void *data)
{
	char str[SAVED_LEN];

	const char *dir = get_desktop_folder();
	char basename_dir[PATH_MAX];
	char filename[PATH_MAX];
	char path[PATH_MAX];

	time_t now = time(NULL);
	struct tm *tm;

	if (!folder_exists(dir))
		dir = get_home_folder();

	if (!folder_exists(dir)) {
		snprintf(str, SAVED_LEN, "%s %s", get_msg(M_SAVE_FAIL),
			dir);
		return;
	}

	tm = localtime(&now);

	snprintf(filename, PATH_MAX, "Biloba-%04d-%02d-%02d-%02d%02d.%02d.blb",
			tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
			tm->tm_hour, tm->tm_min, tm->tm_sec);
	filename[PATH_MAX - 1] = '\0';

	snprintf(path, PATH_MAX, "%s%s%s", dir, DIR_SEP, filename);

	path[PATH_MAX - 1] = '\0';

	if (cur_game) {
		strncpy(basename_dir, dir, sizeof(basename_dir));

		if (replay_dump_game(cur_game, path) == 0)
			snprintf(str, SAVED_LEN, "%s: %s%s\n%s", get_msg(M_SAVED_TO),
				basename(basename_dir), DIR_SEP, filename);
		else
			snprintf(str, SAVED_LEN, "%s:\n%s", get_msg(M_SAVE_FAIL),
				path);

		text_widget_set_text(saved, str);
		widget_show(saved);
	}
}

/**
 * Signal the GUI after clicking the Help button.
 *
 * @param[in]	widget		The Help widget
 * @param[in]	data		Unused callback data
 */
static void do_help(Widget *widget, void *data)
{
	help_start();
}

/**
 * Signal the GUI after clicking the language button.
 *
 * @param[in]	widget		The Play widget
 * @param[in]	data		Unused callback data
 */
static void set_lang(Widget *widget, void *data)
{
	if (!strcmp(langpath,"en")) {
		langpath="fr";
		widget_set_image(lang, "fr.png");
	}
	else if (!strcmp(langpath,"fr")) {
		langpath="es";
		widget_set_image(lang, "es.png");
	}
	else if (!strcmp(langpath,"es")) {
		langpath="en";
		widget_set_image(lang, "en.png");
	}
	gui_reload_images();
}

static void hide_saved(Widget *widget, void *data)
{
	widget_hide(saved);
}

static int server_chooser_get_servers(Widget *widget, char ***strings)
{
	char **servers;
	char **distant_servers;
	int n_distant_servers = 0, i;
	
	n_distant_servers = net_get_servers(&distant_servers);

	servers = malloc((2 + n_distant_servers) * sizeof(char *));
	servers[0] = strdup(get_msg(M_OFFICIAL_SERVER));
	servers[1] = strdup(get_msg(M_LOCAL_SERVER));

	for (i = 0; i < n_distant_servers; i++) {
		servers[i + 2] = strdup(distant_servers[i]);
		free(distant_servers[i]);
	}
	free(distant_servers);
	*strings = servers;

	return 2 + n_distant_servers;
}

static void server_chooser_list_cb(Widget *widget, int index, char *string)
{
	set_server_from_string(string);
	net_stop_getting_servers();

	text_widget_set_text(server, string);
	update_games_list();
}

/**
 * Build the GUI
 */
static void build_gui(void)
{
	int i;

	/* First create all necessary widgets */

	numplayers = 		widget_create("nplay.png", NUM_PLAYERS_MIN_X, PLAYERS_MIN_Y,
					num_players_clicked, NULL);
	numplayers_icon = 	widget_create("2play.png", PLAYERS_MIN_X, PLAYERS_MIN_Y,
					num_players_clicked, NULL);
	title =			widget_create("biloba-title.png", BILOBA_MIN_X, BILOBA_MIN_Y,
					NULL, NULL);
	
	for (i = 0; i < 4; i++) {
		player_pawn_box[i] =	widget_create("empty_40_40.png", PLAYERPAWN_MIN_X(i+1),
					PLAYERPAWN_MIN_Y(i+1), NULL, NULL);
		player_name[i] =	widget_create("empty_200_40.png", PLAYERNAME_MIN_X(i+1),
					PLAYERNAME_MIN_Y(i+1), NULL, NULL);
		player_names[i] = malloc(PLAYER_NAME_LEN + 1);
		snprintf(player_names[i], PLAYER_NAME_LEN, "%s%d", get_msg(M_PLAYER), i + 1);
		text_widget_init(player_name[i], player_names[i], PLAYER_NAME_LEN, 4, 0,
				 player_name_copy, NULL);
	}

	player_pawn[0] = widget_create("pawn-orange.png", PLAYERPAWN_MIN_X(1)+5, PLAYERPAWN_MIN_Y(1)+5,
					NULL, NULL);
	player_pawn[1] = widget_create("pawn-blue.png", PLAYERPAWN_MIN_X(2)+5, PLAYERPAWN_MIN_Y(2)+5,
					NULL, NULL);
	player_pawn[2] = widget_create("pawn-red.png", PLAYERPAWN_MIN_X(3)+5, PLAYERPAWN_MIN_Y(3)+5,
					NULL, NULL);
	player_pawn[3] = widget_create("pawn-green.png", PLAYERPAWN_MIN_X(4)+5, PLAYERPAWN_MIN_Y(4)+5,
					NULL, NULL);

	player_type[0] = widget_create("local.png", PLAYERTYPE_MIN_X(1), PLAYERTYPE_MIN_Y(1),
					update_player_method, NULL);
	player_type[1] = widget_create("local.png", PLAYERTYPE_MIN_X(2), PLAYERTYPE_MIN_Y(2),
					update_player_method, NULL);
	player_type[2] = widget_create("local.png", PLAYERTYPE_MIN_X(3), PLAYERTYPE_MIN_Y(3),
					update_player_method, NULL);
	player_type[3] = widget_create("local.png", PLAYERTYPE_MIN_X(4), PLAYERTYPE_MIN_Y(4),
					update_player_method, NULL);

	server =	 widget_create("server.png", SERVER_MIN_X, SERVER_MIN_Y,
					NULL, NULL);
	text_widget_init(server, get_msg(M_OFFICIAL_SERVER), SERVER_LEN, 111, 0,
			 set_server, NULL);
	
	game_name =	 widget_create("game-name.png", GAMENAME_MIN_X, GAMENAME_MIN_Y,
					NULL, NULL);
	text_widget_init(game_name, get_msg(M_NEWGAME), GAME_NAME_LEN, 111, 0, NULL, NULL);
	
	network_games =	 widget_create("network-games.png", NETWORK_GAMES_LIST_MIN_X, NETWORK_GAMES_LIST_MIN_Y,
					NULL, NULL);
	widget_hide(network_games);
	widget_hide(server);
	widget_hide(game_name);

	for (i = 0; i < 6; i++) {
		network_game[i] = widget_create("empty_200_40.png", GAMELIST_MIN_X(i), GAMELIST_MIN_Y(i),
					NULL, NULL);
		widget_hide(network_game[i]);
		text_widget_init(network_game[i], "", GAME_NAME_LEN, 0, 0, NULL, NULL);
		text_widget_set_editable(network_game[i], FALSE);
	}
	
	create_game =	 widget_create("create-game.png", CREATEGAME_MIN_X, CREATEGAME_MIN_Y,
					do_create_game, NULL);
	join_game =	 widget_create("join-game.png", JOINGAME_MIN_X, JOINGAME_MIN_Y,
					do_join_game, NULL);
	start =	 	 widget_create("start.png", START_MIN_X, START_MIN_Y,
					do_start, NULL);
	save_last = 	 widget_create("save_last.png", SAVE_LAST_MIN_X, SAVE_LAST_MIN_Y,
					do_save_last, NULL);

	help =		 widget_create("help.png", HELP_MIN_X, HELP_MIN_Y,
					do_help, NULL);
	lang =	 	 widget_create("en.png", LANG_MIN_X, LANG_MIN_Y,
					set_lang, NULL);

	saved = 	 widget_create("saved.png", SAVED_MIN_X, SAVED_MIN_Y, hide_saved, NULL);
	text_widget_init(saved, "", SAVED_LEN, 10, 0, NULL, NULL);
	text_widget_set_editable(saved, FALSE);

	widget_hide(saved);

	/* FIXME: Setting server_chooser is done last just because it's the
	 * easiest way to make sure it takes focus over the rest */
	server_chooser = widget_create("choose.png", CHOOSE_SERVER_MIN_X, CHOOSE_SERVER_MIN_Y,
					NULL, NULL);
	list_widget_init(server_chooser, SERVER_MIN_X + (text_widget_get_x_offset(server) - 4),
			 SERVER_MIN_Y,
			 widget_get_width(server) - (text_widget_get_x_offset(server) - 4),
			 server_chooser_get_servers,
			 server_chooser_list_cb);
	widget_hide(server_chooser);

	/* Now set the defaults */
	widget_hide(player_pawn[2]); 	 widget_hide(player_pawn[3]);
	widget_hide(player_type[2]); 	 widget_hide(player_type[3]);
	widget_hide(player_pawn_box[2]); widget_hide(player_pawn_box[3]);
	widget_hide(player_name[2]);   	 widget_hide(player_name[3]);
}

/**
 * Reset all network-related GUI elements
 */
static void reset_net_widgets(void)
{
	network_join_enabled = FALSE;
	network_create_enabled = FALSE;
	update_games_list();
	update_player_types();
	widget_hide(game_name);
	widget_hide(server);
	widget_hide(server_chooser);
}

static int use_net = FALSE;
int options_using_net(void)
{
	return use_net;
}
/**
 * Main GUI loop to get the options
 *
 * @return 0 if the game can start, -1 if the caller should quit.
 */
int get_options(const char *replay_file)
{
	static int initialized = FALSE;
	int net_game_number = -1;
	int i;

	if (!initialized)
	{
		build_gui();
		initialized = TRUE;
	}

	widget_hide(saved);

	if (cur_game && cur_game->move_id > 1)
		widget_show(save_last);
	else
		widget_hide(save_last);

	reset_net_widgets();
	use_net = FALSE;

	if (replay_file) {
		struct stat st;
		int i;

		if (stat(replay_file, &st) != 0 || !S_ISREG(st.st_mode))
			return -1;
	
		if (replay_game_setup(replay_file) < 0)
			return -1;

		set_num_players(replay_get_num_players());
		for (i = 0; i < replay_get_num_players(); i++) {
			text_widget_set_text(player_name[i],
				replay_get_player(i));
			player_types[i] = INPUT_REPLAY;
		}
		return 0;
	}

choose_again:			
	start_clicked = 0;
	do {
		update_gui();
		if (gui_wait_event() < 0)
			return -1;
	} while (!start_clicked);
	
	set_num_players(num_players);

	if (network_create_enabled) {
		use_net = TRUE;
		net_game_number = net_init_game(text_widget_get_text(game_name),
                                num_players, player_types, player_names);
	} else if (network_join_enabled && selected_net_game_id > 0) {
		use_net = TRUE;
		net_game_number = selected_net_game_id;
		net_join(selected_net_game_id, selected_net_game_player_slot,
			 text_widget_get_text(player_name[selected_net_game_player_slot]));
	}

	if (cur_game_mutex == NULL)
		cur_game_mutex = SDL_CreateMutex();

	options_game_lock();

	free_game(cur_game);
	cur_game = init_game();
	cur_game->num_players = num_players;

	for (i = 0; i < num_players; i++)
		cur_game->player_name[i] = strdup(options_get_player_name(i));

	options_game_unlock();

	if (use_net && (net_game_number < 0 || net_wait_ready(num_players, player_names) < 0))
	{
		SDL_FillRect(screen, NULL, 0x00000000);
		SDL_UpdateRect(screen, 0, 0, 0, 0);
		draw_message(get_msg(M_ENOCONN), 200, 284, -1, FALSE);

		llist_for_each(network_games_list, free_game);
		llist_free(network_games_list);
		network_games_list = NULL;

		options_game_lock();
		free_game(cur_game);
		cur_game = NULL;
		options_game_unlock();

		if (delay_with_event_poll(2000) < 0)
			use_net = FALSE;
		else
			goto choose_again;
	}
	if (!use_net)
		net_close();

	llist_for_each(network_games_list, free_game);
	llist_free(network_games_list);
	network_games_list = NULL;

	SDL_FillRect(screen, NULL, 0x00000000);
	SDL_UpdateRect(screen, 0, 0, 0, 0);

	return 0;
}

/**
 * Get a player name
 *
 * @param[in]	i		The player number
 */
const char *options_get_player_name(int i)
{
	return text_widget_get_text(player_name[i]);
}

/**
 * Get a player type
 *
 * @param[in]	i		The player number
 */
int options_get_player_type(int i)
{
	return player_types[i];
}
