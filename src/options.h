/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __OPTIONS_H__
#define __OPTIONS_H__

#include "game.h"

int get_options(const char *replay);
const char *options_get_player_name(int i);
int options_get_player_type(int i);
int options_using_net(void);
Game *options_get_game(void);
void options_game_lock(void);
void options_game_unlock(void);

void replay_get_event(int color, int *x, int *y);

#endif
