/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

/**
 * Biloba - Q1 2005
	* Game by Guillaume Demougeot <dmgt@wanadoo.fr>
	* Code by Colin Leroy <colin@colino.net>
	*
	* This file contains all functions useful to any other
	* file
	*/

#include <SDL.h>
#include <SDL_image.h>
#ifdef MAEMO
#include <SDL_syswm.h>
#include <X11/Xutil.h>
#endif
#ifdef __MINGW32__
#include <windows.h>
#include <shlobj.h>
#endif
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "utils.h"
#include "board.h"
#include "logic.h"
#include "net.h"

char *progpath = NULL;
char *langpath = NULL;

/**
 * Get an object's X coordinate as pixels
 *
 * @param[in] x	The X coordinate as number of tiles
 *
 * @return The corresponding X coord as pixels
 */
int get_x(int x)
{
	return ((50 * x) + X_OFFSET);	
}

/**
 * Get an object's Y coordinate as pixels
 *
 * @param[in] x	The Y coordinate as number of tiles
 *
 * @return The corresponding Y coord as pixels
 */
int get_y(int y)
{
	return ((50 * y) + Y_OFFSET);
}

/**
 * Puts an SDL image on the screen
 *
 * @param[in] surface	The image
 * @param[in] x		The X coord (pixels)
 * @param[in] y		The Y coord (pixels)
 *
 * @attention The screen will be updated only if the board isn't frozen
 */
void put_image(SDL_Surface *surface, int x, int y)
{
	static SDL_Rect *tmp_rect = NULL;
	
	assert(surface != NULL);

	if (tmp_rect == NULL)
		tmp_rect = malloc(sizeof(SDL_Rect));
	tmp_rect->x = x;
	tmp_rect->y = y;
	if (!board_frozen())
		SDL_BlitSurface(surface, NULL, screen, tmp_rect);	
}

static int num_players = 2;

/**
 * Get the number of players in the current game.
 *
 * @return number of players
 */
int game_num_players(void)
{
	return num_players;
}

/**
 * Get whether a game has any network player.
 *
 * @return the number of network players
 */
int game_num_net_players(void)
{
	int n = 0;
	int i;
	
	for (i = 0; i < game_num_players(); i++) {
		Player *player = player_get((PawnColor)i, FALSE, 0);
		if (player->method == INPUT_NETWORK)
			n++;
	}

	return n;
}
/**
 * Set the number of players in the current game.
 *
 * @param[in] num	The number of players
 */
void set_num_players(int num)
{
	assert(num > 1 && num < 5);
	num_players = num;
}

#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif

/**
 * Return minimum
 *
 * @param[in] a		first integer
 * @param[in] b		second integer
 *
 * @return the minimum of a and b
 */
int min (int a, int b)
{
	if (a < b)
		return a;
	else 
		return b;
}

/**
 * Return maximum
 *
 * @param[in] a		first integer
 * @param[in] b		second integer
 *
 * @return the maximum of a and b
 */
int max (int a, int b)
{
	if (a > b)
		return a;
	else 
		return b;
}

/**
 * Checks whether (x,y) is in the box (a,b),(c,b),(a,d),(c,d)
 *
 *    a      c
 *   b.______.b
 *    |      |
 *    |      |
 *    |      |
 *   d.______.d
 *    a      c
 *
 * @param[in] x		x
 * @param[in] y		y
 * @param[in] a		a
 * @param[in] b		b
 * @param[in] c		c
 * @param[in] d		d
 *
 * @return 1 if the point is in the box, 0 otherwise
 */
int is_in_box(int x, int y, int a, int b, int c, int d)
{
#ifdef DEBUG
	printf("is_in_box(%d,%d,%d,%d,%d,%d): (%d,%d,%d,%d)\n",
			x,y,a,b,c,d, min(x,a) == a, min(y,b) == b,
			max(x,c) == c, max(y,d) == d);
#endif
	return (min(x,a) == a && min(y,b) == b &&
		max(x,c) == c && max(y,d) == d);
}

static int playing = FALSE;

/**
 * Sets the playing status
 *
 * @param[in] play	Whether the game is playing
 */
void set_playing(int play) 
{
	playing = play;
}

/**
 * Gets the playing status
 *
 * @return 1 if the game is playing, 0 otherwise
 */
int is_playing(void)
{
	return playing;
}

static int squit = FALSE;

/**
 * Signal that quitting has been requested
 */
static void notify_quit(void)
{
	squit = TRUE;
}

static int handle_ascii(int ascii)
{
#ifndef MAEMO
	switch(ascii) {
	case 't':
		if (!net_is_typing_msg()) {
			net_set_typing_msg(TRUE);
			return 0;
		}
		break;
	case SDLK_RETURN:
	case SDLK_KP_ENTER:
		if (net_is_typing_msg())
			net_set_typing_msg(FALSE);
		return 0;
	}

	if (net_is_typing_msg())
		net_type_msg(ascii);
#endif
	return 0;
}

int handle_generic_event(SDL_Event *event)
{
	int key = 0, mod = 0;

	if (event->type == SDL_KEYDOWN) {
		SDL_KeyboardEvent *kevent = &event->key;
		if (kevent->keysym.sym && kevent->keysym.mod) {
			key = kevent->keysym.sym;
			if (key == SDLK_a)
				key = SDLK_q; /* Ugly hack. */
			mod = kevent->keysym.mod;
		}
	} else if (event->type == SDL_QUIT) {
		key = SDLK_q;
		mod = KMOD_CTRL;
	}

	if (key == SDLK_q && (mod & KMOD_CTRL)) {
		notify_quit();
		if (game_inited())
			end_game();
		return -2;
	}
	if (key == SDLK_e && (mod & KMOD_CTRL) && game_inited()) {
		end_game();
		return -1;
	}
	if (!net_is_typing_msg()) {
		if (key == SDLK_ESCAPE && !game_inited()) {
			notify_quit();
			return -2;
		}
		if (key == SDLK_ESCAPE && game_inited()) {
			end_game();
			return -1;
		}
	}

	if (event->type == SDL_KEYDOWN) {
		SDL_KeyboardEvent *kevent = &event->key;
		if ((kevent->keysym.unicode & 0xFF00) == 0) {
			return handle_ascii(kevent->keysym.unicode & 0xFF);
		}
	}

	return 0;
}

int event_poll(void)
{
	SDL_Event event;

	net_pull_recv_msg();

	while (SDL_PollEvent(&event)) {
		int r = handle_generic_event(&event);
		if (r < 0)
			return r;
	}

	return 0;
}

#define INTERVAL_BETWEEN_CHECKS 50
int delay_with_event_poll(int msec_timeout)
{
	int i, err;
	int num_wait = msec_timeout / INTERVAL_BETWEEN_CHECKS;
	int extra_wait = msec_timeout % INTERVAL_BETWEEN_CHECKS;
	
	for (i = 0; i < num_wait; i++) {
		SDL_Delay(INTERVAL_BETWEEN_CHECKS);
		err = event_poll();
		if (err < 0)
			return err;
	}
	if (extra_wait == 0)
		return 0;

	SDL_Delay(extra_wait);
	return event_poll();
}
		
/**
 * Wait for an SDL event
 *
 * @param[in] event_type	The event type to wait for
 *
 * @return the first event of event_type arrived
 * @attention SDL_USEREVENT_QUIT and SDL_QUIT events are always
 * returned if we got one.
 */
SDL_Event get_sdl_event(int event_type)
{
	SDL_Event event;
	
	do {
		while (TRUE) {
			if (SDL_PollEvent(&event))
				break;
			SDL_Delay(50);
			net_pull_recv_msg();
		}
		if (event.type != event_type
		    && handle_generic_event(&event) < 0) {
			event.type = SDL_USEREVENT_QUIT;
			break;
		}
	} while (event.type != event_type);

	return event;
}


/**
 * Check whether quitting has been requested
 *
 * @return TRUE if quitting has been requested, FALSE otherwise.
 */
int should_quit(void)
{
	return squit;
}

static int is_inited = FALSE;

/**
 * Set the game init status
 *
 * @param bool	Whether the game is inited
 */
void game_init(int bool)
{
	is_inited = bool;
}

/**
 * Return whether the game is initialised
 *
 * @return TRUE if the game is initialised, FALSE otherwise
 */
int game_inited(void)
{
	return is_inited;
}

static int suspend = FALSE;

/**
 * Set the game suspended status
 *
 * @param bool	Whether the game is suspended
 */
void game_suspend(int i)
{
	suspend = i;
}

/**
 * Return whether the game is suspended
 *
 * @return TRUE if the game is suspended, FALSE otherwise
 */
int game_suspended(void)
{
	return suspend;
}

/**
 * Initialise the default language to use, based on the user's
 * environment variable LANG on POSIX platforms, and GetThreadLocale()
 * on Win32.
 */
#ifndef __MINGW32__
void set_language(void)
{
	if (getenv("LANG") == NULL)
		langpath = "en";
	else if (!strncmp(getenv("LANG"), "fr", 2))
		langpath = "fr";
	else if (!strncmp(getenv("LANG"), "en", 2))
		langpath = "en";
	else if (!strncmp(getenv("LANG"), "es", 2))
		langpath = "es";
	else 
		langpath = "en";
}
#else
void set_language(void)
{
	LCID lcid;
	int primary;
	lcid = GetThreadLocale();
	primary = PRIMARYLANGID (LANGIDFROMLCID (lcid));

	if (primary == LANG_FRENCH)
		langpath = "fr";
	else if (primary == LANG_ENGLISH)
		langpath = "en";
	else
		langpath = "en";

}
#endif

/**
 * Load an image from a directory
 *
 * @param[in] prefix	The directory where the image file is
 * @param[in] lang	The language we want
 * @param[in] name	The image file to load
 *
 * @return an SDL_Surface representing the image, or NULL if loading
 * failed.
 */
static SDL_Surface *load_image_prefix(const char *prefix, const char *lang, const char *name)
{
	SDL_Surface *surface = NULL;
	char *path = malloc(strlen(prefix)+ strlen (DIR_SEP) + strlen(name) 
			+ strlen(lang) + strlen(DIR_SEP) + 1);

	strcpy(path, prefix);
	strcat(path, DIR_SEP);
	strcat(path, lang);
	strcat(path, DIR_SEP);
	strcat(path, name);

	surface = IMG_Load(path);
	free(path);

	if (surface == NULL && strcmp(lang, "common"))
		return load_image_prefix(prefix, "common", name);

	return surface;
}

/**
 * Load an image
 *
 * @param[in] name	The image file to load
 *
 * @return an SDL_Surface representing the image, or NULL if loading
 * failed.
 * @attention The image file is looked for first in PREFIX/res/ directory,
 * then in the executable's path if not found.
 */
SDL_Surface *biloba_load_image(const char *name)
{
	SDL_Surface *result = NULL;

	if (langpath == NULL) {
		set_language();
		assert(langpath != NULL);
	}
	
	result = load_image_prefix(PREFIX DIR_SEP "res", langpath, name);
	
	if (!result)
		result = load_image_prefix(PREFIX, langpath, name);
	
	if (!result && progpath != NULL)
		result = load_image_prefix(progpath, langpath, name);
	
	return result;
}

#ifdef MAEMO
/**
 * Set Biloba's application name for the Maemo Window Manager.
 *
 * @param[in] name 		The name to set
 * @param[in] is_fullscreen	Whether we're running fullscreen
 */

void SetWMName(const char *name, int is_fullscreen)
{
        SDL_SysWMinfo info;
        SDL_VERSION(&info.version);
        if ( SDL_GetWMInfo(&info) ) {
                Display *dpy = info.info.x11.display;
                Window win;
		
		if (is_fullscreen)
			win = info.info.x11.fswindow;
		else
			win = info.info.x11.wmwindow;
		XStoreName(dpy, win, name);
        }
}
#endif

static char desktop_path[PATH_MAX] = "";
#ifdef __MINGW32__
/* From GLIB. Thanks guys. */
const char *get_desktop_folder(void)
{
  HRESULT hr;
  LPITEMIDLIST pidl = NULL;
  BOOL b;

  hr = SHGetSpecialFolderLocation (NULL, CSIDL_DESKTOPDIRECTORY, &pidl);
  if (hr == S_OK)
    {
      b = SHGetPathFromIDList (pidl, desktop_path);
      if (b)
	return desktop_path;
    }
  return NULL;
}

const char *get_home_folder(void)
{
	return getenv("USERPROFILE");
}
#else
const char *get_desktop_folder(void)
{
	snprintf(desktop_path, sizeof(desktop_path), "%s/%s",
		 getenv("HOME"), "Desktop");
	desktop_path[PATH_MAX - 1] = '\0';
	return desktop_path;
}

const char *get_home_folder(void)
{
	return getenv("HOME");
}

#endif

int folder_exists(const char *folder)
{
	struct stat st;

	if (stat(folder, &st) < 0)
		return FALSE;
	if (!S_ISDIR(st.st_mode))
		return FALSE;

	return TRUE;
}
