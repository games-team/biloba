/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __UTILS_H__
#define __UTILS_H__

#include <SDL.h>
#include <assert.h>
#include "llist.h"
#include "font.h"

//#define DEBUG 1

#define SDL_USEREVENT_QUIT    (SDL_USEREVENT + 0)
#define SDL_USEREVENT_REFRESH (SDL_USEREVENT + 1)

#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif

#ifndef MAEMO
#define FLAGS (SDL_HWSURFACE | SDL_DOUBLEBUF)
#define XS    800
#define YS    600
#define X_OFFSET 150
#define Y_OFFSET 50
#else
#define FLAGS (SDL_HWSURFACE | SDL_DOUBLEBUF | SDL_FULLSCREEN)
#define XS    800
#define YS    480
#define X_OFFSET 150
#define Y_OFFSET 5
#endif

#define BPP   16

#define PAWN_OFFSET 10
#define ARROW_OFFSET_FROM_CENTER 15
#define G_MSG_X 65
#define G_MSG_Y (YS - 25)

#define G_TALK_X 65
#define G_TALK_Y (YS - 45)
#define MAX_MSG_LEN (((XS -  G_TALK_X) / CHARWIDTH) - 2)

#define HELP_X 10
#define HELP_Y 10
#define MAX_TILES_X 9
#define MAX_TILES_Y 9

#ifndef __MINGW32__
#ifndef MYDATADIR
#define PREFIX "."
#else
#define PREFIX MYDATADIR
#endif
#define DIR_SEP "/"
#else
#define PREFIX "."
#define DIR_SEP "\\"
#endif

#define PLAYER_NAME_LEN	32
#define GAME_NAME_LEN 	32
#define SERVER_LEN 	64
#define SAVED_LEN	256

extern char *progpath;
extern char *langpath;

SDL_Surface * screen;

int get_x(int x);
int get_y(int y);

void put_image(SDL_Surface *surface, int x, int y);

int should_quit(void);
void game_init(int bool);
int game_inited(void);
void game_suspend(int bool);
int game_suspended(void);
int game_num_players(void);
int game_num_net_players(void);
void set_num_players(int num);

#ifndef min
int min (int a, int b);
#endif
#ifndef max
int max (int a, int b);
#endif
int is_in_box(int x, int y, int a, int b, int c, int d);

void set_playing(int playing);
int is_playing(void);

SDL_Event get_sdl_event(int event_type);

SDL_Surface *biloba_load_image(const char *name);

void set_language(void);
int event_poll(void);
int handle_generic_event(SDL_Event *event);
int delay_with_event_poll(int msec_timeout);

const char *get_desktop_folder(void);
const char *get_home_folder(void);
int folder_exists(const char *folder);
#ifdef MAEMO
void SetWMName(const char *name, int is_fullscreen);
#endif

#endif 
