/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

/**
 * Biloba - Q1 2005
 * Game by Guillaume Demougeot <dmgt@wanadoo.fr>
 * Code by Colin Leroy <colin@colino.net>
 *
 * This file contains the network main program code.
 */

#include <stdlib.h>

#ifdef __MINGW32__
#include <winsock2.h>
#endif
#include "server.h"

int main(int argc, char *argv[])
{
	int err;
#ifdef __MINGW32__
        WSADATA wsaData;
        WSAStartup(MAKEWORD(2,0),&wsaData);
#endif	

	err = server_start(argc > 1 ? argv[1] : NULL,
			   argc > 2 ? argv[2] : NULL);

#ifdef __MINGW32__
	WSACleanup();
#endif
	return err;
}
