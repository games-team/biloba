/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

/**
 * Biloba - Q1 2005
 * Game by Guillaume Demougeot <dmgt@wanadoo.fr>
 * Code by Colin Leroy <colin@colino.net>
 *
 * i18n stuff
 */

#include <stdlib.h>
#include <string.h>
#include "msg.h"
#include "utils.h"

int lang = -1;
typedef enum {
	LANG_FR = 0,
	LANG_EN,
	LANG_ES,
	N_LANGS
} LangId;

static const char *msg[N_LANGS][M_NUM_MSGS];

/**
 * Initialise all messages, in all supported languages
 */
static void init_msgs(void)
{
	LangId i;
	
	i = LANG_FR;
	msg[i][M_ROUND]="Au tour de ";
	msg[i][M_DRAW]="Match nul";
	msg[i][M_WINS]=" remporte la partie !";
	msg[i][M_PLAYER_WINS]="Joueur xx remporte la partie !";
	msg[i][M_PLAYER]="Joueur ";
	msg[i][M_WAITING]="Attente de %d joueur%s...";
	msg[i][M_JOINED]="%s nous a rejoint";
	msg[i][M_NEWGAME]="Nouvelle partie";
	msg[i][M_ENOCONN]="Connexion impossible";
	msg[i][M_PLAYER_LEAVES]="Joueur xx abandonne";
	msg[i][M_LEAVES]=" abandonne";
	msg[i][M_YOUR_TURN]="� votre tour, ";
	msg[i][M_OFFICIAL_SERVER]="Officiel";
	msg[i][M_LOCAL_SERVER]="Local";
	msg[i][M_SAVED_TO]="Partie enregistr�e dans";
	msg[i][M_SAVE_FAIL]="Impossible d'enregistrer dans";
	
	i = LANG_EN;
	msg[i][M_ROUND]="Up to ";
	msg[i][M_DRAW]="Draw";
	msg[i][M_WINS]=" wins !";
	msg[i][M_PLAYER_WINS]="Player xx wins!";
	msg[i][M_PLAYER]="Player ";
	msg[i][M_WAITING]="Waiting for %d player%s";
	msg[i][M_JOINED]="%s joined";
	msg[i][M_NEWGAME]="New game";
	msg[i][M_ENOCONN]="Connection failed";
	msg[i][M_PLAYER_LEAVES]="Player xx leaves";
	msg[i][M_LEAVES]=" leaves";
	msg[i][M_YOUR_TURN]="Up to you, ";
	msg[i][M_OFFICIAL_SERVER]="Official";
	msg[i][M_LOCAL_SERVER]="Local";
	msg[i][M_SAVED_TO]="Saved game to";
	msg[i][M_SAVE_FAIL]="Couldn't save game to";

	i = LANG_ES;
	msg[i][M_ROUND]="Turno de ";
	msg[i][M_DRAW]="Empate";
	msg[i][M_WINS]=" victorias";
	msg[i][M_PLAYER_WINS]="Jugador xx gana";
	msg[i][M_PLAYER]="Jugador ";
	msg[i][M_WAITING]="Esperando por %d jugador%s";
	msg[i][M_JOINED]="%s se ha unido";
	msg[i][M_NEWGAME]="Nuevo juego";
	msg[i][M_ENOCONN]="Fallo al conectar";
	msg[i][M_PLAYER_LEAVES]="Jugador xx abandona";
	msg[i][M_LEAVES]=" abandona";
	msg[i][M_YOUR_TURN]="A tu turno, ";
	msg[i][M_OFFICIAL_SERVER]="Oficial";
	msg[i][M_LOCAL_SERVER]="Local";
	msg[i][M_SAVED_TO]="Juego guardado en";
	msg[i][M_SAVE_FAIL]="No se pudo guardar el juego";
}

/**
 * Gets a message
 *
 * @param[in] id	The message we want
 *
 * @return The corresponding message, in the user's language.
 */
const char *get_msg(MsgId id)
{
	if (lang < 0) {
		set_language();
		assert(langpath != NULL);
		init_msgs();
	}

	if (!strcmp(langpath, "fr"))
		lang = LANG_FR;
	else if (!strcmp(langpath, "en"))
		lang = LANG_EN;
	else if (!strcmp(langpath, "es"))
		lang = LANG_ES;

	assert (lang >= 0);

	return msg[lang][id];
}

