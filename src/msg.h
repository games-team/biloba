/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __MSG_H__
#define __MSG_H__

typedef enum {
	M_ROUND = 0,
	M_DRAW,
	M_WINS,
	M_PLAYER_WINS,
	M_PLAYER,
	M_WAITING,
	M_JOINED,
	M_NEWGAME,
	M_ENOCONN,
	M_PLAYER_LEAVES,
	M_LEAVES,
	M_YOUR_TURN,
	M_OFFICIAL_SERVER,
	M_LOCAL_SERVER,
	M_SAVED_TO,
	M_SAVE_FAIL,
	M_NUM_MSGS
} MsgId;

const char *get_msg(MsgId id);

#endif 
