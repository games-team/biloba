biloba (0.9.3-11) unstable; urgency=medium

  * Update protocol in homepage and source URLs
  * Update years range in copyright
  * Update Standards-Version to 4.6.2, no other changes
  * Update watch version to 4

 -- Ricardo Mones <mones@debian.org>  Fri, 13 Jan 2023 20:53:10 +0100

biloba (0.9.3-10) unstable; urgency=high

  * Add patch to fix FTBFS with gcc 10 (Closes: #957039)
  * Add Rules-Requires-Root field
  * Update Standards-Version to latest (no other changes)
  * Bump compat level to 13 and simplify rules
  * Clean lintian I: debian-rules-parses-dpkg-parsechangelog

 -- Ricardo Mones <mones@debian.org>  Thu, 03 Sep 2020 16:28:08 +0200

biloba (0.9.3-9) unstable; urgency=medium

  * Change manpages format to reduce Build-Depends
  * Bump compat level up to 12
  * Update Standards-Version to 4.4.1 (no other changes)

 -- Ricardo Mones <mones@debian.org>  Sat, 02 Nov 2019 19:53:00 +0100

biloba (0.9.3-8) unstable; urgency=medium

  * debian/control
  - Update Vcs-* URls to point to salsa
  - Update Standards-Version to latest (no other changes)
  * debian/control, debian/compat
  - Set debhelper compat level to 11

 -- Ricardo Mones <mones@debian.org>  Thu, 16 Aug 2018 02:07:06 +0200

biloba (0.9.3-7) unstable; urgency=medium

  * debian/patches/fix_spelling_error_net.c.patch
  - Add patch to fix spelling error in binary
  * debian/control, debian/rules, debian/compat
  - Standards-Version: update to 4.1.2
  - Compatibility level raised to 10
  - Build-Depends: remove autotools-dev (not needed anymore)
  - Remove --with autotools_dev from dh call (not needed anymore)
  - Enable all hardening options
  - Reformat long descriptions and remove trailing whitespace
  * debian/copyright
  - Update year ranges and use format URL with https

 -- Ricardo Mones <mones@debian.org>  Tue, 26 Dec 2017 00:40:53 +0100

biloba (0.9.3-6) unstable; urgency=medium

  * debian/patches/define_PATH_MAX_for_Hurd.patch
  - Add patch to fix FTBFS in hurd-i386
  * debian/menu
  - Removed to implement tech-ctte resolution on #741573
  * debian/biloba.xpm debian/biloba.install
  - Remove also XPM icon which was only required for menu
  * debian/control
  - Standards-Version: update to 3.9.8 (no other changes required)
  - Vcs-*: updated to use https URLs
  - Add Multi-Arch: foreign to biloba-data (hinted by Multi-Arch hinter)
  * debian/biloba.xml, debian/biloba-server.xml
  - Fix location of GPL-2 file in manpages.

 -- Ricardo Mones <mones@debian.org>  Fri, 21 Oct 2016 18:35:01 +0200

biloba (0.9.3-5) unstable; urgency=medium

  * debian/control
  - Update Vcs-* headers to use the canonical hostname
  - Update Standards-Version to 3.9.5 (no other changes)
  * debian/copyright
  - Update year range up to current
  * debian/biloba.desktop
  - Add Keywords entry (thanks lintian!)

 -- Ricardo Mones <mones@debian.org>  Mon, 02 Jun 2014 01:57:55 +0200

biloba (0.9.3-4) unstable; urgency=low

  * debian/compat, debian/control
  - Bump required compatibility level to 9
  - Update Standards-Version to 3.9.3 (no other changes)
  * debian/rules
  - Remove override_dh_auto_configure stanza
  * debian/copyright
  - Update to Copyright Format 1.0 and years

 -- Ricardo Mones <mones@debian.org>  Thu, 07 Jun 2012 18:25:27 +0200

biloba (0.9.3-3) unstable; urgency=low

  * debian/rules, debian/*.install, debian/clean
  - Migrate rules to debhelper sequencer
  * debian/compat, debian/control
  - Bump required compatibility level to 8
  * debian/copyright
  - Update field names to follow current DEP5 format

 -- Ricardo Mones <mones@debian.org>  Sat, 07 Jan 2012 23:58:30 +0100

biloba (0.9.3-2) unstable; urgency=low

  * debian/rules
  - Provide build-arch and build-indep targets (thanks lintian)
  * debian/control
  - Update Standards-Version to 3.9.2 (no other changes required)
  - Development moved to git.debian.org: added Vcs-* headers

 -- Ricardo Mones <mones@debian.org>  Sun, 14 Aug 2011 13:10:33 +0200

biloba (0.9.3-1) unstable; urgency=low

  * New upstream version
  * debian/control
  - Updated Standards-Version to 3.9.1 (no other changes)
  * debian/biloba-server.xml, debian/rules, debian/manpages
  - Added manpage for new binary
  * debian/biloba.xml
  - Fix homepage address in manual page too
  * debian/docs
  - Added NEWS file

 -- Ricardo Mones <mones@debian.org>  Thu, 10 Mar 2011 11:38:08 +0100

biloba (0.6-5) unstable; urgency=low

  * Switch to dpkg-source 3.0 (quilt) format
  * debian/rules
  - Use build system config.{sub|guess} scripts
  * debian/control
  - Updated Standards-Version to 3.8.4, no other changes
  - Added ${misc:Depends} to -data for keeping lintian happy
  * debian/copyright
  - Updated in accordande with DEP5 and fixed date ranges

 -- Ricardo Mones <mones@debian.org>  Sun, 20 Jun 2010 13:37:42 +0200

biloba (0.6-4) unstable; urgency=low

  * debian/control
  - Fix missing homepage (Closes: #561825)
  - Add ${misc:Depends} to binary (thanks lintian)
  * debian/biloba.desktop
  - Added asturian translation for comment

 -- Ricardo Mones <mones@debian.org>  Wed, 30 Dec 2009 13:12:22 +0100

biloba (0.6-3) unstable; urgency=low

  * debian/control
  - Fix -data description and rework it a bit (Closes: #550336)
  - Bumped Standards-Version to 3.8.3, no other changes required
  * debian/rules
  - Removed deprecated dh_desktop (thanks lintian)

 -- Ricardo Mones <mones@debian.org>  Fri, 09 Oct 2009 12:55:53 +0200

biloba (0.6-2) unstable; urgency=low

  * debian/control
  - Bumped Standards-Version, no changes required
  * debian/copyright
  - Switched to machine-parsable format
  * debian/biloba.desktop
  - Added Spanish and French (thanks Tristan Chabredier) translations
  * debian/watch
  - Added watch file

 -- Ricardo Mones <mones@debian.org>  Wed, 25 Jun 2008 15:16:37 +0200

biloba (0.6-1) unstable; urgency=low

  * New upstream release
  * debian/control
  - fix board shape description (Closes: #460518)

 -- Ricardo Mones <mones@debian.org>  Fri, 18 Jan 2008 15:08:11 +0100

biloba (0.5-1) unstable; urgency=low

  * New upstream release
  * debian/biloba.xml
  - Update manpage to include command line option
  * debian/rules, debian/biloba.dirs, debian/biloba.desktop
  - Added the desktop file for visibility on games menu

 -- Ricardo Mones <mones@debian.org>  Fri, 11 Jan 2008 00:02:02 +0100

biloba (0.4-3) unstable; urgency=low

  * debian/control
  - Updated Standards-Version to current 3.7.3
  - Made Homepage a proper control field
  - Removed unused misc:Depends
  * debian/rules
  - Added PNG icon from source (renamed to match package)

 -- Ricardo Mones <mones@debian.org>  Wed, 09 Jan 2008 10:17:30 +0100

biloba (0.4-2) unstable; urgency=low

  * debian/control
  - Fixed Source-Version usage (Closes: #432949)
    (patch by Lior Kaplan <kaplan@debian.org>)
  * debian/rules
  - Fixed -$(MAKE) distclean usage

 -- Ricardo Mones <mones@debian.org>  Wed, 18 Jul 2007 23:24:31 +0200

biloba (0.4-1) unstable; urgency=low

  * Initial release (Closes: #329955)

 -- Ricardo Mones <mones@debian.org>  Wed, 26 Jul 2006 23:24:39 +0200
